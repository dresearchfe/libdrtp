/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/time.h>

#include "drtp.h"
#include "test.h"

static char frameBuffer[128*1024];

static void * memory_cb (struct drtp_stream_handle_t *handle, uint32_t *len) {
	(void) handle;
	*len = sizeof(frameBuffer);
	return frameBuffer;
}

static enum drtp_status client_cb (struct drtp_stream_handle_t *handle, uint32_t flags, 
                                            uint32_t cam, uint32_t width, uint32_t height, 
                                            uint64_t ts, void* data, uint32_t len, struct rtp_with_subheaders_t * header) {
    static uint32_t bytes;
    (void) data, (void) handle, (void)header;
    if (flags & DRTP_FRAME_MEM_REALLOC)
	{
		void *mem;
		uint32_t memLen = (len + 64*1024-1) & ~(64*1024-1);
		if (data) mem = realloc(data, memLen);
		else mem = malloc(memLen);
		if (mem) {
			drtp_set_receive_buffer(handle, mem, memLen);
			return DRTP_SUCCESS;
		}
		return DRTP_ERR_NO_MEM;
	}

    bytes += len;
    if (flags & DRTP_PACKET_LOST)
        printf("FRAME(s) LOST IN TRANSMISSION\n");

    if (flags & DRTP_FRAME_DONE) {
        printf("\rFrame on cam %u, %ux%u, %u bytes, ts %llu.         ", 
                cam, width, height, bytes, ts);
        fflush(stdout);
        bytes=0;
    }
    return DRTP_SUCCESS;
}
/* --- */

void start_receiving(char * from, struct drtp_stream_handle_t * s, int die, int tcp) {
    enum drtp_status ret;
	if (tcp) {
		if (DRTP_SUCCESS != ( ret = drtp_tcp_stream_start(s, client_cb, from) )) {
			printf("drtp_tcp_stream_start: %d (%s)\n", ret, drtp_strerror(ret));
			if (die) 
				exit(1);
		}
	} else {
		if (DRTP_SUCCESS != ( ret = drtp_udp_stream_start(s, client_cb, memory_cb, from, 1, 720, 288) )) {
			printf("drtp_stream_start: %d (%s)\n", ret, drtp_strerror(ret));
			if (die) 
				exit(1);
		}
	}
}
/* --- */
        
void stop_receiveing(struct drtp_stream_handle_t * s, int die, int tcp) {
	enum drtp_status ret;
	if (DRTP_SUCCESS != ( ret = drtp_stream_stop(s) )) {
		printf("stop %s stream: %d (%s)\n", (tcp ? "TCP" : "UDP"), ret, drtp_strerror(ret));
		if (die) 
			exit(1);
	}
}

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 * 
 * NO CODE BELOW THIS! */
