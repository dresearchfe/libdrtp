/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 */


#ifndef FRAMES_H
#define FRAMES_H
struct frames_t {
    unsigned int len;
    uint8_t * data;
};

extern const struct frames_t frames[];
extern unsigned int frames_count;

#endif /* defined FRAMES_H */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 * 
 * NO CODE BELOW THIS! */
