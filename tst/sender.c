/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/time.h>

#include "drtp.h"
#include "test.h"

sem_t go;
unsigned int clients_present = 0;

static enum drtp_status server_cb(uint32_t cam, enum drtp_request req, uint32_t width, uint32_t height, const char* ip_addr) {

    printf("\nCB: %s cam %u, req type %u, %ux%u\n", (ip_addr ? ip_addr : ""), cam, req, width, height);
    switch (req) {
		case drtp_req_info:
			clients_present ++;
			break;
        case drtp_req_start:
            clients_present ++;
            if (clients_present == 1)
                sem_post(&go);
            break;
        case drtp_req_stop:
            clients_present --;
        default:
            break;
    }

    return DRTP_SUCCESS;
}
/* --- */

static uint64_t _ts(void) {
    struct timeval now;
    uint64_t ret;
    gettimeofday(&now, NULL);
    ret = now.tv_sec;
    ret *= 1000;
    ret += now.tv_usec / 1000UL;
    return ret;
}
/* --- */

void start_server(int tcp) {
    enum drtp_status ret;
	if (tcp) {
		drtp_start_rtp_tcp_server(server_cb);
	} else {
		if (DRTP_SUCCESS != ( ret = drtp_set_req_cb(server_cb, NULL) )) {
			printf("drtp_set_req_cb: %d (%s)\n", ret, drtp_strerror(ret));
			exit(1);
		}
	}
}
/* --- */
void stop_server(int tcp) {
    enum drtp_status ret;
	if (tcp) {
		if (DRTP_SUCCESS != ( ret = drtp_stop_rtp_tcp_server() )) {
			printf("drtp_stop_rtp_tcp_server: %d (%s)\n", ret, drtp_strerror(ret));
		}
	} else {
		if (DRTP_SUCCESS != ( ret = drtp_set_req_cb(NULL, NULL) )) {
			printf("drtp_set_req_cb: %d (%s)\n", ret, drtp_strerror(ret));
		}
	}
}
/* --- */

void send_frame(unsigned cam, unsigned w, unsigned h, uint8_t * data, uint32_t len, int jpeg, unsigned usleep_time) {
    uint64_t ts = _ts();
    enum drtp_status ret;
	static int report = 0;

	if (usleep_time) {
		if ( 0 > usleep( usleep_time ))
			perror("usleep()");
	}

    printf("\rSending frame (%u bytes, ts %llu) to %u receivers", len, ts, clients_present);
    fflush(stdout);

    if (jpeg)
    {
    	drtp_jpeg_specs_t specs;
    	specs.type = 0;
    	specs.q = 75;
    	specs.width = w / 8;
    	specs.height = h / 8;
    	ret = drtp_send_jpeg_rtp(cam, w, h, ts, data, len, &specs);
    }
    else
    {
    	ret = drtp_send_h264_tcp(cam, w, h, ts, data, len);
    }

    if (DRTP_SUCCESS !=  ret) {
		clients_present = 0;
		if (!report) {
			report = 1;
			printf("drtp_send_frame: %d (%s)\n", ret, drtp_strerror(ret));
			if (ret == DRTP_ERR_NO_CONN) 
				printf("Continuning anyway\n");
			else
				exit(1);
		}
    } else { 
		report = 0;
	}
}

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 * 
 * NO CODE BELOW THIS! */
