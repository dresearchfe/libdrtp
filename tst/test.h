/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 */

#ifndef TEST_H
#define TEST_H

#include <semaphore.h>
extern sem_t go;
extern unsigned int clients_present;

void start_server(int tcp);
void stop_server(int tcp);
void send_frame(unsigned cam, unsigned w, unsigned h, uint8_t * data, uint32_t len, int jpeg, unsigned usleep_time);

void start_receiving(char * from, struct drtp_stream_handle_t * s, int die, int tcp);
void stop_receiveing(struct drtp_stream_handle_t * s, int die, int tcp);

#endif

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 * 
 * NO CODE BELOW THIS! */
