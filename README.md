libdrtp
=======

A rtp based communication library from [DResearch Fahrzeugelektronik GmbH](http://www.dresearch-fe.de).
This library is used to transfer jpeg images via udp based rtp.

