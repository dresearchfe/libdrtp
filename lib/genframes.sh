#!/bin/sh
# SPDX-License-Identifier: LGPL-3.0-only
#
# Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
#

i=0

cat << EOF
#include <stdint.h>

struct frame_t {
    unsigned int len;
    uint8_t * data;
};
EOF

echo -en "\nconst struct frame_t frames[] = {"
for file in "$1"/*; do 
    echo "$file" >&2
    f=$(basename "$file")
    s=$(stat -c%s "$file")
    echo -en "\n\t { $s, (uint8_t *) \"" 
    ./hex.sh "$file" 
    echo -n "\"},"
    ((i++))
done

echo -e "\n};\n unsigned int frames_count = $i;"
