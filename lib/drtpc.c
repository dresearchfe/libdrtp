/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp client implemetnation
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <poll.h>

#include <glib.h>
#include <errno.h>
#include <string.h>
#include <drtp.h>
#include "drtp_internal.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#else
#define VERSION drtp_VERSION
#endif


/*
 * PRIVATE FUNCTIONS
 */

static enum drtp_status _send_request(int s, struct rtcp_header_t * req) {
	enum drtp_status ret = DRTP_SUCCESS;

	if (sizeof(*req) != send(s, req, sizeof(*req), 0)) {
		if (    (errno == ENOTCONN)
				||  (errno == ECONNREFUSED)
				||  (errno == EHOSTDOWN)
				||  (errno == EHOSTUNREACH)
				||  (errno == ECONNRESET) )
			ret = DRTP_ERR_NO_CONN;
		else
			ret = DRTP_ERR_SYS;
		LOG_PERROR("send()");
	}

	return ret;
}
/* --- */

static enum drtp_status _start_request(struct drtp_stream_handle_t * s) {
	struct rtcp_header_t start;

	_gen_rtcp_stream_request(&start, s->ssrc);
	return _send_request(s->sock, &start);
}
/* --- */

static enum drtp_status _stop_request(struct drtp_stream_handle_t * s) {
	struct rtcp_header_t stop;

	_gen_rtcp_bye_request(&stop, s->ssrc);
	return _send_request(s->sock, &stop);
}
/* --- */

static enum drtp_status _keepalive_request(struct drtp_stream_handle_t * s) {
	struct rtcp_header_t alive;

	_gen_rtcp_alive_request(&alive, s->ssrc);
	return _send_request(s->sock, &alive);
}
/* --- */

static enum drtp_status _stop_and_close(struct drtp_stream_handle_t * stream) {
	enum drtp_status ret, ret2;

	if (0 != stream->ssrc)
	{
		ret = _stop_request(stream);
	} else {
		ret = DRTP_SUCCESS;
	}

	ret2 = _close_socket(stream->sock);
	stream->sock = -1;

	return (ret == DRTP_SUCCESS) ? ret2 : ret;
}
/* --- */


/* RSR 20131118 copy free reimplementation */
static void * _udp_thread(void * p)
{
	struct drtp_stream_handle_t *stream = (struct drtp_stream_handle_t *) p;
	char scratch[sizeof(struct rtp_with_subheaders_t)];
	size_t scratchSize = 0;
	uint32_t stream_cam, stream_w, stream_h; /* requested values */
	unsigned seq_lost = 0;
	uint16_t frame_start_seq = 0; /* frame start sequence */
	drtp_memory_cb mem_cb = stream->mem_cb;
	drtp_frame_cb frame_cb = stream->cb;

	if (!mem_cb || !frame_cb)
	{
		LOG_ERROR("Thread started with NULL callback! Aborting.");
		return NULL;
	}
	_decode_ssrc(stream->ssrc, &stream_cam, &stream_w, &stream_h);
	stream->frameFlags = DRTP_PACKET_LOST; /* no incomplete first frame */

	while(!stream->shutdown)
	{
		struct rtp_with_subheaders_t header;
		ssize_t  received;
		uint32_t len, flags, cam, w, h;
		uint64_t ts;
		uint16_t exp_seq;

		if (!stream->frameBuf)
		{
			stream->frameBuf = mem_cb(stream, &stream->frameBufLen);
			stream->frameBufPos = 0;
			scratchSize = 0;
			if (!stream->frameBuf) { break; } /* regular connection shutdown */
		}

		len = stream->frameBufLen - stream->frameBufPos;
		if (len > scratchSize)
		{
			if (stream->sock < 0)
			{
				if (DRTP_SUCCESS != (_open_sender_socket( &stream->sock, &stream->remote, 0 )))
				{
					LOG_PERROR("%s cam %u %ux%u open udp sender: receiver thread terminates", __func__, stream_cam, stream_w, stream_h);
					break;
				}
				_set_sock_bufs(stream->sock, 0, DRTPC_RECEIVE_BUFFER_PER_SOCKET);
				_start_request(stream);
			}

			received = recv(stream->sock, &stream->frameBuf[stream->frameBufPos], len, MSG_DONTWAIT);
			if (received < 0)
			{
				if (_fatal_receive_error()) {
					if (stream->sock >= 0)
					{
						LOG_PERROR("%s cam %u %ux%u recv(fd:%d len:%u): receive thread terminates",
								__func__, stream_cam, stream_w, stream_h, stream->sock, stream->frameBufLen - stream->frameBufPos);
					}
					break;
				}
				struct pollfd pfd = { stream->sock, POLLIN, 0 };
				switch (poll(&pfd, 1, 50))
				{
					case 0: continue; /* timeout */
					case 1: continue; /* data to receice */
					case -1: LOG_PERROR("%s cam %u %ux%u udp(%d) poll", __func__, stream_cam, stream_w, stream_h, stream->sock); break;
					default: LOG_ERROR("%s cam %u %ux%u udp poll(%d) unexpected result", __func__, stream_cam, stream_w, stream_h, stream->sock); break;
				}
				continue;
			}
		} else { /* this should not happen, see EOD check bellow */
			LOG_ERROR("%s cam %u %ux%u udp receive buffer overrun seq:%hu-%hu lost:%u - reset buffer", __func__, stream_cam, stream_w, stream_h, frame_start_seq, stream->seq, seq_lost);
			stream->frameBufPos = 0;
			scratchSize = 0;
			continue;
		}

		if ( DRTP_SUCCESS != _parse_rtp(&stream->frameBuf[stream->frameBufPos], received, &header.rtp, &ts, &flags ) ) {
			LOG_ERROR("%s cam %u %ux%u Error parsing incoming RTP packet size %d/%u", __func__, stream_cam, stream_w, stream_h, received, len);
			stream->frameFlags |= DRTP_PACKET_LOST;
			continue;
		}
		_decode_ssrc(header.rtp.ssrc, &cam, &w, &h);
		if (cam != stream_cam) {
			LOG_ERROR("%s cam %u %ux%u incoming RTP packet for cam %u %ux%u", __func__, stream_cam, stream_w, stream_h, cam, w, h);
			stream->frameFlags |= DRTP_PACKET_LOST;
			continue;
		}
		if (stream->frameBufPos + received > stream->frameBufLen)
		{  /* HYP-19008 */
			LOG_ERROR("%s cam %u %ux%u size error: received:%d requested %u on position %u of %u - reset buffer",
				   	__func__, stream_cam, stream_w, stream_h, received, len, stream->frameBufPos, stream->frameBufLen);
			stream->frameBufPos = 0;
			scratchSize = 0;
			continue;
		}

		exp_seq = stream->seq + 1;
		if ((0 != stream->seq) && (exp_seq != header.rtp.sequence)) {
			stream->frameFlags |= DRTP_PACKET_LOST;
			seq_lost += header.rtp.sequence - exp_seq;
			LOG_ERROR("%s cam %u %ux%u Frame(s) frame cam %u %ux%u dropped! Expected seq %hu, got seq %hu lost:%u",
					__func__, stream_cam, stream_w, stream_h, cam, w, h, exp_seq, header.rtp.sequence, seq_lost);
		}
		stream->seq = header.rtp.sequence;

		switch (DRTP_CODEC_FROM_FLAGS( flags ))
		{
			case codec_jpeg:
				if ( DRTP_SUCCESS != _parse_jpeg(&stream->frameBuf[stream->frameBufPos + sizeof(header.rtp)], received - sizeof(header.rtp), &flags, &header.sub.jpeg ) ) {
					LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Error parsing JPEG header of incoming RTP packet size %d/%u", __func__, stream_cam, cam, stream_w, stream_h, w, h, received, len);
					stream->frameFlags |= DRTP_PACKET_LOST;
					continue;
				}

				if ((w != header.sub.jpeg.bit.width * 8)  || (h != header.sub.jpeg.bit.height * 8))
				{
					// share with different resolution
					w = header.sub.jpeg.bit.width  * 8;
					h = header.sub.jpeg.bit.height * 8;
				}

				if (scratchSize && !(flags & DRTP_FRAME_START))
				{ /* copy frame content back to header area */
					if ((stream->frameBufPos + scratchSize) != header.sub.jpeg.bit.fragm_offs)
					{
						/* HYP-21656 reducing trace flooding
						LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Error parsing JPEG fragment expected offset:%u received:%u",
							   __func__, stream_cam, cam, stream_w, stream_h, w, h, stream->frameBufPos + scratchSize,  header.sub.jpeg.bit.fragm_offs); */
						stream->frameFlags |= DRTP_PACKET_LOST;
						/* this creates random content for the misssing and the current fragment. The current fragment could be moved, but we throw away the
						 * defect frames anyway so keep it. Just fix the offset position within the frame buffer! */
						stream->frameBufPos = header.sub.jpeg.bit.fragm_offs - scratchSize + received; /* want point after fragment end, len is rtp-header + fragment length */
						if (stream->frameBufPos > stream->frameBufLen) { stream->frameBufPos = stream->frameBufLen; }
					} else {
						memcpy(&stream->frameBuf[stream->frameBufPos], scratch, scratchSize);
						stream->frameBufPos += received;  /* we points after the fragment end now */
					}
				} else { /* frame start: move frame area back to the start, drop earlier fragments */
					if (scratchSize)
					{
						LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) frame lost - end marker not seen seq:%hu-%hu lost:%u",
								__func__, stream_cam, cam, stream_w, stream_h, w, h, frame_start_seq, stream->seq, seq_lost);
					}
					frame_start_seq = stream->seq;
					seq_lost = 0;
					stream->frameFlags = 0;
					scratchSize = sizeof(header.rtp) + sizeof(header.sub.jpeg); /* len >= scratchSize because of parsing */
					if (scratchSize > sizeof(scratch)) /* RSR: static check */
					{
						LOG_ERROR("Illegal scratch size %u max:%u", scratchSize, sizeof(scratch));
					}
					if (0 != header.sub.jpeg.bit.fragm_offs)
					{
						/* HYP-21656 reducing trace flooding
						LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Error parsing JPEG start fragment expected offset:0 received:%u",
								__func__, stream_cam, cam, stream_w, stream_h, w, h, header.sub.jpeg.bit.fragm_offs); */
						stream->frameFlags |= DRTP_PACKET_LOST;
						seq_lost = 1;
					}
					memmove(&stream->frameBuf[0], &stream->frameBuf[stream->frameBufPos + scratchSize], received - scratchSize);
					stream->frameBufPos = received - scratchSize;
				}
				break;
			default:
				LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Unsupported codec %d in RTP packet.", __func__, stream_cam, cam, stream_w, stream_h, w, h, DRTP_CODEC_FROM_FLAGS( flags ));
				scratchSize = 0;
				break;
		}

		if (stream->frameBufPos >= stream->frameBufLen - scratchSize -2)
		{ /* buffer overrun */
			stream->frameFlags |= DRTP_PACKET_LOST;
			stream->frameBuf[stream->frameBufPos-2] = 0xff;
			stream->frameBuf[stream->frameBufPos-1] = 0xd9; /* jpeg EOD marker */
			LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Frame buffer overrun, cut frame", __func__, stream_cam, cam, stream_w, stream_h, w, h);
			flags |= DRTP_FRAME_DONE;
		}

		if (flags & DRTP_FRAME_DONE)
		{
			frame_cb(stream, stream->frameFlags | flags, cam, w, h, ts, stream->frameBuf, stream->frameBufPos, &header);
			stream->frameBufPos = stream->frameBufLen = 0;
			stream->frameBuf = 0;
			scratchSize = 0;
			if (stream->sock >= 0)
			{
				enum drtp_status ret = _keepalive_request(stream);
				if (ret != DRTP_SUCCESS)
				{
					LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Sending keepalive packet failed.", __func__, stream_cam, cam, stream_w, stream_h, w, h);
					break;
				}
			}
		} else {
			if (stream->frameBufPos > scratchSize)
			{
				memcpy(scratch, &stream->frameBuf[stream->frameBufPos - scratchSize], scratchSize);
				stream->frameBufPos -= scratchSize;
			} else {
				LOG_ERROR("%s cam %u/%u %ux%u(%ux%u) Bad frame size %u - drop.", __func__, stream_cam, cam, stream_w, stream_h, w, h, stream->frameBufPos);
				scratchSize = 0;
				stream->frameFlags |= DRTP_PACKET_LOST;
			}
		}
	}
	if (!stream->shutdown) { _stop_and_close(stream); }
	return NULL;
}
/* --- */


/** Helper function to guarantee the len to read or an error */
static gboolean _tcp_read(struct drtp_stream_handle_t *stream, void *data, size_t len, const char* trace)
{
	size_t read_len;
	ssize_t res;
	for(read_len = 0; !stream->shutdown && (read_len < len); read_len += res) {
		res = recv(stream->sock, (uint8_t*)data + read_len, len - read_len, 0);
		if (res <= 0) {
			if (stream->sock >= 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) { res = 0; continue; }
			LOG_PERROR( "Read error(%zd, '%s') (%s) read %zu/%zu - restart stream", res, strerror(errno), trace, read_len, len);
			return 0;
		}
	}
	return !stream->shutdown;
}

static void * _tcp_thread(void * p) {
	struct drtp_stream_handle_t *stream = (struct drtp_stream_handle_t *) p;
	drtp_frame_cb frame_cb = stream->cb; /* be conservative with the cb handle */
	int notify = 0;                      /* avoid missing network notification floodings */
	uint16_t seq[DRTP_MAX_CAMS];         /* expected RTP sequence numbers */

	if (!frame_cb) {
		LOG_ERROR("Thread started with NULL callback! Aborting");
		return NULL;
	}
	if (    (DRTP_SUCCESS !=
				frame_cb(stream, DRTP_FRAME_MEM_REALLOC, 0, 0, 0, 0,
					stream->frameBuf, 64*1024, NULL))
			|| (stream->frameBufLen < 64*1024)) {
		LOG_ERROR("Thread started without frame buffer! Aborting");
		return NULL;
	}

	while (!stream->shutdown) { /* reader loop: check socket and read frame loop */
		uint32_t frame_len;       /* size of the complete H.264 frame send to the user */
		uint32_t ssrc;            /* cam, width, height */
		uint64_t dsa_ts = 0;      /* RTP (90kHz based) and DSA timestamp (based on DRES SAI) */
		enum drtp_status ret;
		uint32_t cam, w, h;

		if (stream->sock < 0) {  /* (re)start the socket (if lost) */
			if (DRTP_SUCCESS != (ret = _open_sender_socket( &stream->sock, &stream->remote, 1))) {
				if (stream->shutdown) { break; }
				if (0 == notify++) {
					LOG_PERROR("open server");
				}
				sleep(3);
				continue; /* auto connect retry */
			} else {
				notify = 0;
				memset(seq, 0, sizeof(seq));
				_set_sock_bufs(stream->sock, 0, DRTPC_RECEIVE_BUFFER_PER_SOCKET);
			}
		}

		if (!_tcp_read(stream, &frame_len, sizeof(frame_len),
					"read TCP frame length error: restart stream")) {
			close(stream->sock); stream->sock = -1; continue;
		}

		if (!_tcp_read(stream, &ssrc, sizeof(ssrc),
					"read TCP ssrc error: restart stream")) {
			close(stream->sock); stream->sock = -1; continue;
		}
		_decode_ssrc(ssrc, &cam, &w, &h);

		if (!_tcp_read(stream, &dsa_ts, sizeof(dsa_ts),
					"read TCP DSA timestamp error: restart stream")) {
			close(stream->sock); stream->sock = -1; continue;
		}

		if (    (frame_len > stream->frameBufLen)
				&&  (    (DRTP_SUCCESS !=
						frame_cb(stream, DRTP_FRAME_MEM_REALLOC, 0, 0, 0, 0,
							stream->frameBuf, frame_len, NULL))
					|| (stream->frameBufLen < frame_len))           ) {

			LOG_ERROR("Unable to allocate %u bytes frame memory - restart stream.",
					frame_len);
			close(stream->sock); stream->sock = -1; continue;
		}

		if (!_tcp_read(stream, stream->frameBuf, frame_len,
					"read TCP FRAME error: restart stream")) {
			close(stream->sock); stream->sock = -1; continue;
		}

		/* user notification */
		frame_cb(stream, DRTP_FRAME_START | DRTP_FRAME_DONE,
				cam, w, h, dsa_ts, stream->frameBuf, frame_len, NULL);
	}

	if (!stream->shutdown) { _stop_and_close(stream); }
	return NULL;
}
/* --- */


/*
 * PUBLIC FUNCTIONS
 */

const char* drtp_version() { return VERSION; }

enum drtp_status _drtpc_init(void) {
	return DRTP_SUCCESS;
}


enum drtp_status drtp_set_receive_buffer(struct drtp_stream_handle_t * handle, void *buf, uint32_t bufLen)
{
	if (!handle)
		return DRTP_ERR_INVALID_ARG;
	handle->frameBuf = buf;
	handle->frameBufLen = bufLen;
	return DRTP_SUCCESS;
}

#define RTPC_MAGIC 0x11111137


/* --- */
enum drtp_status drtp_udp_stream_start(struct drtp_stream_handle_t * handle,
                                        drtp_frame_cb callback, drtp_memory_cb mem_cb, const char * remote_addr,
										uint32_t cam, uint32_t width, uint32_t height) {
	enum drtp_status ret;

	if (!handle || !callback || !mem_cb)
		return DRTP_ERR_INVALID_ARG;

	if (    (DRTP_SUCCESS != (ret = _str_to_sockaddr( remote_addr, &handle->remote, DRTP_SERVER_COMMAND_PORT)))
			||  (DRTP_SUCCESS != (ret = _open_sender_socket( &handle->sock, &handle->remote, 0 ))))
	{
		LOG_PERROR("open sender");
		return ret;
	}

	_set_sock_bufs(handle->sock, 0, DRTPC_RECEIVE_BUFFER_PER_SOCKET);

	handle->seq = 0;
	handle->ssrc = _encode_ssrc(cam, width, height);
	handle->cb = callback;
	handle->mem_cb = mem_cb;
	handle->frameBuf = 0;
	handle->frameBufLen = 0;

	handle->thread = g_thread_new("Urtp", _udp_thread, handle);

	if (DRTP_SUCCESS != (ret = _start_request( handle ))) {
		GThread *thread = handle->thread; handle->thread = NULL;
		handle->shutdown = TRUE;
		if (thread) { g_thread_join(thread); }
		return ret;
	}

	handle->magic = RTPC_MAGIC;
	return DRTP_SUCCESS;
}
/* --- */

enum drtp_status drtp_tcp_stream_start(struct drtp_stream_handle_t * handle,
		drtp_frame_cb callback, const char * remote_addr) {
	enum drtp_status ret;

	if (!handle || !callback)
		return DRTP_ERR_INVALID_ARG;

	memset(handle, 0, sizeof(struct drtp_stream_handle_t));
	if (DRTP_SUCCESS != (ret = _str_to_sockaddr( (remote_addr ? remote_addr : "127.0.0.1"), &handle->remote, (remote_addr ? DRTP_SERVER_PORT : (DRTP_SERVER_PORT+2)))))
		return ret;

	handle->magic = RTPC_MAGIC;
	handle->sock = -1;
	handle->cb = callback;

	handle->thread = g_thread_new("Trtp", _tcp_thread, handle);
	return DRTP_SUCCESS;
}
/* --- */

enum drtp_status drtp_stream_restart(struct drtp_stream_handle_t * handle) {
	if (!handle)
		return DRTP_ERR_INVALID_ARG;
	if (handle->magic != RTPC_MAGIC)
		return DRTP_ERR_NOT_INITIALIZED;
	if (handle->shutdown)
		return DRTP_ERR_NO_CONN;
	if (0 == handle->ssrc) {
		int s = handle->sock; handle->sock = -1;
		if (s < 0) { return DRTP_ERR_NO_CONN; }
		close(s);
		return DRTP_SUCCESS;
	} else {
		enum drtp_status ret  = _stop_request(handle);
		handle->shutdown = TRUE;
		GThread *thread = handle->thread; handle->thread = NULL;
		if (handle->sock >= 0) { int s = handle->sock; handle->sock = -1; _close_socket(s); }
		if (thread) { g_thread_join(thread); }
		if (handle->sock >= 0) { int s = handle->sock; handle->sock = -1; _close_socket(s); }
		if (DRTP_SUCCESS != (ret = _open_sender_socket( &handle->sock, &handle->remote, 0 )))
		{
			LOG_PERROR("open sender");
			return ret;
		}
		_set_sock_bufs(handle->sock, 0, DRTPC_RECEIVE_BUFFER_PER_SOCKET);
		handle->frameBuf = 0;
		handle->frameBufLen = 0;
		handle->shutdown = FALSE;
		handle->thread = g_thread_new("Urtp", _udp_thread, handle);
		if (DRTP_SUCCESS != (ret = _start_request( handle ))) {
			GThread *thread = handle->thread; handle->thread = NULL;
			handle->shutdown = TRUE;
			if (thread) { g_thread_join(thread); }
			return ret;
		}

		return DRTP_SUCCESS;
	}
}
/* --- */

enum drtp_status drtp_stream_stop(struct drtp_stream_handle_t * handle) {
	enum drtp_status ret = DRTP_SUCCESS;
	if (!handle)
		return DRTP_ERR_INVALID_ARG;
	if (handle->magic != RTPC_MAGIC)
		return DRTP_ERR_NOT_INITIALIZED;
	if (0 != handle->ssrc) {
		ret  = _stop_request(handle);
	}
	handle->shutdown = TRUE;
	GThread *thread = handle->thread; handle->thread = NULL;
	if (handle->sock >= 0) { int s = handle->sock; handle->sock = -1; _close_socket(s); }
	if (thread) { g_thread_join(thread); }
	if (handle->sock >= 0) { int s = handle->sock; handle->sock = -1; _close_socket(s); }
	handle->magic = 0;
	return ret;
}
/* --- */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
