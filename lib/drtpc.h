/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp client interface
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

#ifndef _DRTPC_
#define _DRTPC_

#define DRTPC_RECEIVE_BUFFER_PER_SOCKET (1 * 1024 * 1024)

#include <glib.h>
#include <arpa/inet.h>

#include "drtp_rtp.h"

/* bits 1-3 : frame infos */
#define	DRTP_FRAME_START       (1<<0)  /**< data will start a new frame - only available for DRTP_CODEC_H264 */
#define	DRTP_FRAME_DONE        (1<<1)  /**< data ends the current frame  */
#define	DRTP_PACKET_LOST       (1<<2)  /**< previous packet(s) was/were lost in transmission */
#define	DRTP_FRAME_MEM_REALLOC (1<<3)  /**< data is the last set memory pointer, len the size request (realloc) */

/* bits 5-8: codec type */
#define DRTP_CODEC_SHIFT           4
#define DRTP_CODEC_BITS            3
#define DRTP_CODEC_MASK            \
    ( ((1 << DRTP_CODEC_BITS) - 1) << DRTP_CODEC_SHIFT)

#define DRTP_CODEC_FROM_FLAGS( flags )  \
    ( ((flags) & DRTP_CODEC_MASK) >> DRTP_CODEC_SHIFT )

#define DRTP_SET_CODEC_FLAG( codec_type, flags ) \
    (flags) = \
             ( (codec_type << DRTP_CODEC_SHIFT) & DRTP_CODEC_MASK ) \
             | ((flags) &  ~DRTP_CODEC_MASK )

/* currently there are 3 bits available for codec info allowing a total of 7 codec types */
enum drtp_codec_t {
    codec_h264     = 1,
    codec_jpeg     = 2,
    codec_unknown3 = 3,
    codec_unknown4 = 4,
    codec_unknown5 = 5,
    codec_unknown6 = 6,
    codec_unknown7 = 7
};

struct drtp_stream_handle_t; /* forward for typedefs */

/** The frame callback type definition; used by drtp_stream_start.
  * Callback will be run upon receiving an RTP frame. Multiple RTP frames
  *  form a picture. The flag DRTP_FRAME_START is set on picture
  *  start; DRTP_FRAME_DONE is set on picture end.
  * \param handle pointer to a struct stream handle filled by the library
  * \param flags
  * \param cam camera "address" number range [1..DRTP_MAX_CAMS]
  * \param width, \param height image resolution
  * \param timestamp 64bit timestamp from dsaHeader
  * \param data frame data
  * \param len size of frame data (in bytes)
  * \param header rtp header + subheader structure
  * At least for TCP the frames are send completely, requesting more memory using DRTP_FRAME_MEM_REALLOC.
  * RTP frames are send partially using DRTP_FRAME_START, DRTP_FRAME_DONE with internal memory.
  */
typedef enum drtp_status (*drtp_frame_cb)(struct drtp_stream_handle_t *handle, uint32_t flags,
                                            uint32_t cam, uint32_t width, uint32_t height,
                                            uint64_t timestamp, void* data, uint32_t len,
                                            struct rtp_with_subheaders_t * header);

/** Frame malloc callback type definition; used by drtp_stream_start.
  * \param handle pointer to a struct stream handle filled by the library
  * \param len pointer filled out with size of memory
  * \return memory pointer or NULL
  */
typedef void* (*drtp_memory_cb)(struct drtp_stream_handle_t * handle, uint32_t *len);

/** This is an opaque readonly structure, never change in user context! */
struct drtp_stream_handle_t {
	uint32_t magic;               /**< internal type check */
	struct sockaddr_in remote;    /**< address of the remote server */
	int sock;                     /**< socket used for remote communication */
	uint32_t ssrc;                /**< stream identifier or 0 for TCP */
	GThread *thread;              /**< receive thread */
	volatile gboolean shutdown;   /**< receive thread shutdown request */
	drtp_frame_cb cb;             /**< user callback function */
	drtp_memory_cb mem_cb;        /**< user memory callback function */
	uint16_t seq;                 /**< sequence number for frame verification */
	uint8_t  *frameBuf;           /**< receive buffer (TCP only) */
	uint32_t frameBufLen;         /**< size of the receive buffer */
	uint32_t frameBufPos;         /**< receive buffer write position */
	uint32_t frameFlags;          /**< flags collected via RTP fragments (mainly frame lost) */
};

/** Set the receive buffer used for incoming RTP/TCP frames.
  * Should be called with memory request callback (DRTP_FRAME_MEM_REALLOC) using realloc
  * \param handle pointer to a struct stream handle filled by the library
  * \param buf pointer to buffer
  * \param bufLen buffer length
  */
enum drtp_status drtp_set_receive_buffer(struct drtp_stream_handle_t * handle, void *buf, uint32_t bufLen);

/** Request a stream start (RT[C]P/UDP) and register a frame processing callback with the library.
  * \param handle pointer to a struct stream handle filled by the library
  * \param callback frame callback to be registered
  * \param remote_addr string containing IP address and port of the remote host, e.g. "19.44.23.42:12345"
  * \param cam requested camera "address" number range [1..DRTP_MAX_CAMS]
  * \param width, \param height requested image resolution
  * \param context user defined context
  * Works independently of TCP clients.
  */
enum drtp_status drtp_udp_stream_start(struct drtp_stream_handle_t * handle,
                                        drtp_frame_cb callback, drtp_memory_cb mem_cb, const char * remote_addr,
                                        uint32_t cam, uint32_t width, uint32_t height);

/** Request a stream start (RTP/TCP) and register a frame processing callback with the library.
  * \param handle pointer to a struct stream handle filled by the library
  * \param callback frame callback to be registered
  * \param remote_addr String containing IP address and port of the remote host, e.g. "19.44.23.42:12345"
  * Any frame of the mulitpexed transfered RTP/TCP-stream is returned using the callback.
  */
enum drtp_status drtp_tcp_stream_start(struct drtp_stream_handle_t * handle,
                                        drtp_frame_cb callback, const char * remote_addr);

/** (Re-)Send a "start stream" request (RT[C]P/UDP) on an initialized handle.
  * This can be done multiple times even when the stream is already active as the server part
  * of the library ignores duplicate start requests.
  * It is not implemented for TCP streams.
  * \param handle pointer to a struct stream handle filled by the library
  */
enum drtp_status drtp_stream_restart(struct drtp_stream_handle_t * handle);

/** Request a stream stop und unregister the frame callback.
  * Can be used for UDP and TCP streams.
  * \param handle pointer to a struct stream handle filled by the library
  */
enum drtp_status drtp_stream_stop(struct drtp_stream_handle_t * handle);

#endif /* _DRTPC_ */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
