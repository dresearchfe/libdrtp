/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp header
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

#ifndef _DRTP_RTP_
#define _DRTP_RTP_

#define RTP_MAX_PAYLOAD_PER_PACKET  1400

/*
 * RFC 2435 JPEG over RTP
 */

/* RFC 1889 payload type, see http://www.faqs.org/rfcs/rfc1890.html */
#define RTP_TYPE_JPEG             26

/* RFC 2435 JPEG over RTP header, see http://www.faqs.org/rfcs/rfc2435.html */
struct rtp_subheader_jpeg_t {
    struct {
        unsigned type_spec:  8;
        unsigned fragm_offs: 24;
        unsigned type:       8;
        unsigned q:          8;
        unsigned width:      8;
        unsigned height:     8;
    } __attribute__ ((packed)) bit;
} __attribute__ ((packed));

/* RFC 3550 standard RTP header without optional CSRC, see http://www.faqs.org/rfcs/rfc3550.html */
struct rtp_header_t {
    union{
        uint16_t all;
        struct {
            unsigned type:      7; /*  9- 15 */
            unsigned marker   : 1; /*      8 */
            unsigned c_sources: 4; /*  4-  7 */
            unsigned extension: 1; /*      3 */
            unsigned padding:   1; /*      2 */
            unsigned version:   2; /*  0-  1 */
        } __attribute__ ((packed)) bit;
    } bits;
    unsigned sequence: 16; /* 16- 31 */
    unsigned timestamp:32; /* 32- 63 */
    unsigned ssrc:     32; /* 64- 95 */
} __attribute__ ((packed));

/* Comfort struct */
struct rtp_with_subheaders_t {
    struct rtp_header_t rtp;
    union {
        struct rtp_subheader_jpeg_t jpeg;
        /* more subheaders to be added here  */
    } sub;
};

/* RFC 2032 RTCP header, see http://www.faqs.org/rfcs/rfc2032.html */
struct rtcp_header_t {
    union {
        uint16_t all;
        struct {
            unsigned type:      8; /* 8-15 */
            unsigned mbz:       5; /* 3- 7 */
            unsigned padding:   1; /*    2 */
            unsigned version:   2; /* 0- 1 */
        }__attribute__((packed)) bit;
    } bits;
    unsigned len:      16; /*16-31 */
    unsigned ssrc:     32; /*32-63 */
} __attribute__ ((packed));

#define RTCP_TYPE_FIR   192 /* RFC 2032 full intra request, see http://www.faqs.org/rfcs/rfc2032.html */
#define RTCP_TYPE_RR    201 /* RFC 3550 receiver report; see http://www.faqs.org/rfcs/rfc3550.html */
#define RTCP_TYPE_BYE   203 /* RFC 3550 BYE, see http://www.faqs.org/rfcs/rfc3550.html */
#define RTCP_TYPE_APP   204 /* RFC 3550 application defined, see http://www.faqs.org/rfcs/rfc3550.html */

#define RTCP_APPDEF_SUBTYPE_INFO    9   /* subtype for "INFO" packet */

#endif /* _DRTP_RTP_ */
/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
