/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp server implementation
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

#include <stdio.h>
#include <semaphore.h>
#include <glib.h>
#include <errno.h>

#include <sched.h>

#include <sys/time.h>
#include <sys/stat.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include <drtp.h>
#include "drtp_internal.h"

static volatile uint32_t initialised = 0;
static volatile drtp_req_cb request_callback_tcp = NULL;
static volatile drtp_req_cb request_callback_udp = NULL;
static sem_t lock;

static GThread *listener_thread_tcp;
static GThread *listener_thread_udp;
static volatile gboolean listener_active_tcp = FALSE;
static volatile gboolean listener_active_udp = FALSE;


/** RTPC socket UDP/TCP, server socket TCP for listen */
static int listener_socket_tcp = -1;
static int listener_socket_udp = -1;

typedef enum {
	RECV_UDP = 0,   /* UDP receiver slot */
	RECV_TCP = 1    /* TCP receiver slot */
} recv_type_t;

struct receivers_t {
	int     socket;
	time_t  last_seen;
	struct  sockaddr_in dest;
	unsigned width;
	unsigned height;
	uint32_t ssrc;
};

static struct streams_t {
	uint16_t sequence;
	uint64_t total_frames;
	struct receivers_t receivers[2][DRTP_MAX_CONN_PER_CAM];
	unsigned num_udp_receivers;
	unsigned num_tcp_receivers;
	struct drtp_cam_info_t stats[2][DRTP_MAX_CONN_PER_CAM];
} streams [DRTP_MAX_CAMS];



/*
 * PRIVATE FUNCTIONS
 */

static inline struct streams_t * _strm(unsigned cam_num) {
	return &streams[cam_num - 1];
}
/* --- */

static enum drtp_status _close_listener_socket_tcp() {
	int s = listener_socket_tcp;
	listener_socket_tcp = -1;
	return _close_socket(s);
}
/* --- */

static enum drtp_status _close_listener_socket_udp() {
	int s = listener_socket_udp;
	listener_socket_udp = -1;
	return _close_socket(s);
}
/* --- */

/* Request processing functions */

static enum drtp_status _call_app (recv_type_t tcp_udp, uint32_t cam, enum drtp_request req, uint32_t width, uint32_t height, const char *ip_addr) {
	drtp_req_cb cb = (tcp_udp == RECV_TCP  ? request_callback_tcp : request_callback_udp);
	return cb ? cb(cam, req, width, height, ip_addr) : DRTP_ERR_UNKNOWN;
}
/* --- */

static unsigned int _udp_stream_active(uint32_t ssrc) {
	unsigned active = 0;
	unsigned int i;
	uint32_t cam, w, h;
	_decode_ssrc(ssrc, &cam, &w, &h);

	for(i=0; i<DRTP_MAX_CONN_PER_CAM; i++)
		if ((-1 < _strm(cam)->receivers[RECV_UDP][i].socket) && (_strm(cam)->receivers[RECV_UDP][i].ssrc  == ssrc))
			active++;

	return active;
}
/* --- */

static enum drtp_status _find_empty_udp(uint32_t cam, unsigned int * index) {
	unsigned int i;

	for(i=0; i<DRTP_MAX_CONN_PER_CAM; i++)
		if (-1 == _strm(cam)->receivers[RECV_UDP][i].socket) {
			*index = i;
			return DRTP_SUCCESS;
		}

	return DRTP_ERR_BUSY;
}
/* --- */

static enum drtp_status _find_receiver_udp(uint32_t ssrc, struct sockaddr_in * incoming, unsigned int * index) {
	unsigned int i;
	uint32_t cam, w, h;

	_decode_ssrc(ssrc, &cam, &w, &h);

	for(i=0; i<DRTP_MAX_CONN_PER_CAM; i++) {
		if (    (-1 < _strm(cam)->receivers[RECV_UDP][i].socket) &&
				((incoming == &_strm(cam)->receivers[RECV_UDP][i].dest) || (
					(_strm(cam)->receivers[RECV_UDP][i].ssrc                == ssrc) &&
					(_strm(cam)->receivers[RECV_UDP][i].dest.sin_addr.s_addr == incoming->sin_addr.s_addr) &&
					(_strm(cam)->receivers[RECV_UDP][i].dest.sin_port        == incoming->sin_port)))) {

			*index = i;
			return DRTP_SUCCESS;
		}
	}

	return DRTP_ERR_INVALID_ARG;
}
/* --- */

static enum drtp_status _stop_streaming_udp(uint32_t ssrc, struct sockaddr_in * incoming, uint32_t * run_callback) {
	unsigned int i;
	struct drtp_cam_info_t * stats;
	uint32_t cam, w, h;

	_decode_ssrc(ssrc, &cam, &w, &h);

	if (DRTP_SUCCESS != _find_receiver_udp(ssrc, incoming, &i))
		return DRTP_SUCCESS;

	stats     = &_strm(cam)->stats[RECV_UDP][i];
	stats->last_stopped = time(NULL);
	stats->state = inactive;

	_strm(cam)->receivers[RECV_UDP][i].socket = -1;  /* == listener_socket */
	_strm(cam)->num_udp_receivers--;

	if (!_udp_stream_active(ssrc))
		*run_callback = 1;

	return DRTP_SUCCESS;
}
/* --- */

static enum drtp_status _teardown_stream_udp(uint32_t cam, uint32_t ssrc, struct sockaddr_in * dest) {
	char ip_addr[64];
	enum drtp_status ret;
	uint32_t run_callback = 0;

	if (DRTP_SUCCESS != (ret = _stop_streaming_udp(ssrc, dest, &run_callback))) {
		LOG_ERROR("Error closing connection: %d (%s)\n", ret, drtp_strerror(ret));
		return ret;
	}

	if (run_callback)
		return _call_app(RECV_UDP, cam, drtp_req_stop, 0, 0,  inet_ntop(AF_INET, &dest->sin_addr, ip_addr, sizeof(ip_addr)));

	return DRTP_SUCCESS;
}
/* --- */

static enum drtp_status _check_and_start_streaming_udp(uint32_t ssrc, struct sockaddr_in * incoming,
		uint32_t *run_callback) {
	unsigned int i;
	enum drtp_status ret;
	struct receivers_t * recv;
	struct drtp_cam_info_t * recv_stats;
	uint32_t cam, w, h;

	_decode_ssrc(ssrc, &cam, &w, &h);

	if (DRTP_SUCCESS == _find_receiver_udp(ssrc, incoming, &i)) {
		_strm(cam)->receivers[RECV_UDP][i].last_seen = time(NULL);
		LOG_ERROR("_check_and_start_streaming_udp cam %u already open", cam);
		return DRTP_ERR_ALREADY_IN_PROG; /* stream is already open */
	}

	for(i = 0; i < DRTP_MAX_CONN_PER_CAM; i++)
	{
		if ((-1 < _strm(cam)->receivers[RECV_UDP][i].socket))
		{
			if (((_strm(cam)->receivers[RECV_UDP][i].last_seen + DRTP_KEEPALIVE_TIMEOUT) < time(0)))
			{
				LOG_ERROR( "inactive connection cam:%u %ux%u found, closing", cam,  _strm(cam)->receivers[RECV_UDP][i].width,  _strm(cam)->receivers[RECV_UDP][i].height);
				_teardown_stream_udp(cam, ssrc, &_strm(cam)->receivers[RECV_UDP][i].dest);
			}
		}
	}

	if (DRTP_SUCCESS != ( ret = _find_empty_udp(cam, &i) ) )
	{
		LOG_ERROR("_check_and_start_streaming_udp cam %u no free connection", cam);
		return ret;
	}

	if (!_udp_stream_active(ssrc))
	{
		*run_callback = 1;
	} else {
		// LOG_ERROR("_check_and_start_streaming_udp cam %u stream running on other connection", cam); // RSR yes, we support multiple connections, no warning...
	}

	recv            = &_strm(cam)->receivers[RECV_UDP][i];
	recv->width     = w;
	recv->height    = h;
	recv->ssrc      = ssrc;
	recv->socket    = listener_socket_udp;
	recv->dest      = *incoming;
	recv->last_seen = time(NULL);

	recv_stats               = &_strm(cam)->stats[RECV_UDP][i];
	memset(recv_stats, 0x00, sizeof(*recv_stats));
	recv_stats->state        = active;
	recv_stats->last_started = time(NULL);

	/* (re-)initialise sequence counter upon first start request */
	if (0 == _strm(cam)->num_udp_receivers)
		_strm(cam)->sequence = random() & 0xFFFF;
	_strm(cam)->num_udp_receivers++;

	for(i = 0; i < DRTP_MAX_CONN_PER_CAM; i++)
	{
		if (recv ==  &_strm(cam)->receivers[RECV_UDP][i]) { continue; }
		if (-1 < _strm(cam)->receivers[RECV_UDP][i].socket)
		{
			if ((_strm(cam)->receivers[RECV_UDP][i].ssrc == ssrc) && (_strm(cam)->receivers[RECV_UDP][i].dest.sin_addr.s_addr == incoming->sin_addr.s_addr)) {
				LOG_ERROR( "connection cam:%u %ux%u from new requested ip found, ign", cam,  _strm(cam)->receivers[RECV_UDP][i].width,  _strm(cam)->receivers[RECV_UDP][i].height);
				_teardown_stream_udp(cam, ssrc, &_strm(cam)->receivers[RECV_UDP][i].dest);
			}
		}
	}

	return DRTP_SUCCESS;
}
/* --- */

static enum drtp_status _handle_keepalive_udp(uint32_t ssrc, struct sockaddr_in * incoming) {
	enum drtp_status ret;
	unsigned int i;
	uint32_t cam, w, h;

	_decode_ssrc(ssrc, &cam, &w, &h);

	if (DRTP_SUCCESS != (ret = _find_receiver_udp(ssrc, incoming, &i)) )
	{
		 LOG_ERROR("keep alive cam %u %ux%u - no active conenction\n", cam, w, h);
		return DRTP_SUCCESS;
	}

	_strm(cam)->receivers[RECV_UDP][i].last_seen = time(NULL);

	return DRTP_SUCCESS;
}
/* --- */

static enum drtp_status _send_info(uint32_t cam, struct sockaddr_in * incoming) {
	(void) cam, (void) incoming;
	return DRTP_ERR_NOT_IMPLEMENTED;
}
/* --- */

static enum drtp_status _process_req(struct sockaddr_in sender, enum drtp_request req, uint32_t ssrc) {
	enum drtp_status ret;
	uint32_t cam, w, h, run_callback=0;

	_decode_ssrc(ssrc, &cam, &w, &h);

	if (cam > DRTP_MAX_CAMS)
		return DRTP_ERR_INVALID_ARG;

	if ( sem_wait( &lock ) ) {
		LOG_PERROR("sem_wait");
		return DRTP_ERR_SYS;
	}

	switch (req) {
		case drtp_req_start: ret = _check_and_start_streaming_udp(ssrc, &sender, &run_callback); break;
		case drtp_req_stop : ret = _stop_streaming_udp(ssrc, &sender, &run_callback); break;
		case drtp_req_info : ret = _send_info(ssrc, &sender); break;
		case drtp_req_alive: ret = _handle_keepalive_udp(ssrc, &sender); break;
		default:
							 LOG_ERROR("Unknown request type %u\n", req);
							 ret = DRTP_ERR_INVALID_ARG;
	}

	if ( sem_post( &lock ) ) {
		LOG_PERROR("sem_post");
		return DRTP_ERR_SYS;
	}

	if (ret != DRTP_SUCCESS)
		return ret;

	if(run_callback)
	{
		char ip_addr[64];
		return _call_app(RECV_UDP, cam, req, w, h, inet_ntop(AF_INET, &sender.sin_addr, ip_addr, sizeof(ip_addr)));
	}

	return DRTP_SUCCESS;
}
/* --- */

static enum drtp_status _teardown_socket(int sock)
{
	int cam, connection;
	for(cam = 0; cam < DRTP_MAX_CAMS; cam++)
	{
		for(connection = 0; connection < DRTP_MAX_CONN_PER_CAM; connection++)
		{
			if (streams[cam].receivers[RECV_UDP][connection].socket == sock)
			{
				streams[cam].receivers[RECV_UDP][connection].socket = -1;
				streams[cam].num_udp_receivers--;
			}
			if (streams[cam].receivers[RECV_TCP][connection].socket == sock)
			{
				streams[cam].receivers[RECV_TCP][connection].socket = -1;
				streams[cam].num_tcp_receivers--;
			}
		}
	}
	return _close_socket(sock);
}
/* --- */

/* The listener thread + friends.
 * This thread will listen for incoming requests on the command socket,
 * then run the callback upon receiving a request.
 */

static int _fatal_processing_error(enum drtp_status err) {
	switch (err) {
		case DRTP_ERR_SYS:
		case DRTP_ERR_UNKNOWN:
			return 1;
		default: break;
	}
	return 0;
}
/* --- */

static void * _UDPthread(void * warranty) {
	(void) warranty;

	while(listener_active_udp)
	{
		uint8_t data[128];
		ssize_t len;
		struct sockaddr_in sender;
		socklen_t addrlen = sizeof(sender);
		enum drtp_status ret;
		enum drtp_request req;
		uint32_t ssrc;

		if (listener_socket_udp < 0)
		{
			ret = _open_listener_socket(NULL, DRTP_SERVER_COMMAND_PORT, SOCK_DGRAM, &listener_socket_udp);
			_set_sock_bufs(listener_socket_udp, DRTPS_SEND_BUFFER, DRTPS_RECEIVE_BUFFER);
		}
		len = recvfrom(listener_socket_udp, data, sizeof(data), 0,
				(struct sockaddr *)&sender, &addrlen);

		if (!listener_active_udp) { break; }
		if ( (0 >= len) && _fatal_receive_error()) {
			LOG_PERROR("recvfrom (restart listener socket)");
			_close_socket(listener_socket_udp); listener_socket_udp = -1;
			continue;
		}

		if (DRTP_SUCCESS != (ret = _parse_rtcp(data, (unsigned) len, &req, &ssrc))) {
			LOG_ERROR("Error parsing incoming RTCP from %s:%u; error %d. Ignoring.\n",
					inet_ntoa(sender.sin_addr),
					ntohs(sender.sin_port), ret);
			continue;
		}

		if (DRTP_SUCCESS != (ret = _process_req(sender, req, ssrc))) {
			LOG_ERROR("Error processing incoming request (type %u, ssrc %x) from %s:%u; error %d (%s).\n",
					req, ssrc, inet_ntoa(sender.sin_addr),
					ntohs(sender.sin_port), ret, drtp_strerror(ret));
			if (_fatal_processing_error(ret))
			{
				_close_socket(listener_socket_udp); listener_socket_udp = -1;
			}
		}
	}
	_close_listener_socket_udp();
	return NULL;
}
/* --- */

static void * _TCPthread(void * _port) {
	unsigned port = GPOINTER_TO_UINT(_port);
	int notify = 0;


	while(listener_active_tcp) {
		struct sockaddr_in sender;
		int cam, clientFD;
		socklen_t addrlen = sizeof(sender);
		char ip_addr[64];

		if (listener_socket_tcp < 0) {
			if ( sem_wait( &lock ) ) {
				LOG_PERROR("sem_wait");
				break; /* fatal */
			}
			if (DRTP_SUCCESS != _open_listener_socket(NULL, port, SOCK_STREAM, &listener_socket_tcp))
			{
				if (!notify) { notify = 1; LOG_PERROR("FATAL: recvfrom (listener_thread will TERMINATE!)"); }
				listener_socket_tcp = -1;
			} else {
				notify = 0;
			}
			if ( sem_post( &lock ) ) {
				LOG_PERROR("sem_post");
				break; /* fatal */
			}
		}
		if (listener_socket_tcp < 0) { sleep(5); continue; }

		clientFD = accept(listener_socket_tcp, (struct sockaddr *)&sender, &addrlen);

		if (0 > clientFD) {
			if (!listener_active_tcp) { break; }
			LOG_PERROR("accept failed, restart RTP/TCP server");
			_close_listener_socket_tcp();
			continue;
		}
		if (!listener_active_tcp) { _teardown_socket(clientFD); break; }

		_set_sock_bufs(clientFD, DRTPS_SEND_BUFFER, 0);
		int started = 0;
		int stopped = 0;
		for(cam = 1; cam <= DRTP_MAX_CAMS; cam++)
		{
			unsigned int i;
			struct receivers_t *recv = 0;
			for(i=0; i<DRTP_MAX_CONN_PER_CAM; i++) {
				if ((_strm(cam)->receivers[RECV_TCP][i].socket >= 0) && (_strm(cam)->receivers[RECV_TCP][i].dest.sin_addr.s_addr == sender.sin_addr.s_addr))
				{
					close(_strm(cam)->receivers[RECV_TCP][i].socket); _strm(cam)->receivers[RECV_TCP][i].socket = -1;
					stopped++;
				}
				if (!recv && (-1 == _strm(cam)->receivers[RECV_TCP][i].socket)) {
					recv = &_strm(cam)->receivers[RECV_TCP][i];
					struct drtp_cam_info_t * recv_stats = &_strm(cam)->stats[RECV_TCP][i];
					recv->width     = 0;
					recv->height    = 0;
					recv->ssrc      = 0;
					recv->dest      = sender;
					recv->last_seen = time(NULL);
					recv->socket    = clientFD;
					memset(recv_stats, 0x00, sizeof(*recv_stats));
					recv_stats->state        = active;
					recv_stats->last_started = time(NULL);
					/* (re-)initialise sequence counter upon first start request */
					if (0 == _strm(cam)->num_tcp_receivers) { _strm(cam)->sequence = random() & 0xFFFF; }
					_strm(cam)->num_tcp_receivers++;
					started++;
				}
			}
			if (!recv) {
				LOG_ERROR("no more server connections for cam %d\n", cam);
			}
		}
		if (stopped) { _call_app(RECV_TCP, 0, drtp_req_stop, 0, 0, inet_ntop(AF_INET, &sender.sin_addr, ip_addr, sizeof(ip_addr))); }
		if (started) { _call_app(RECV_TCP, 0, drtp_req_info, 0, 0, inet_ntop(AF_INET, &sender.sin_addr, ip_addr, sizeof(ip_addr))); } else { _teardown_socket(clientFD); }
	}
	_close_listener_socket_tcp();
	return NULL;
}
/* --- */


static enum drtp_status _start_rtcp_listener(void) {
	enum drtp_status ret = DRTP_SUCCESS;

	ret = _open_listener_socket(NULL, DRTP_SERVER_COMMAND_PORT, SOCK_DGRAM, &listener_socket_udp);

	_set_sock_bufs(listener_socket_udp, DRTPS_SEND_BUFFER, DRTPS_RECEIVE_BUFFER);

	if (ret == DRTP_SUCCESS) {
		listener_active_udp = TRUE;
		listener_thread_udp = g_thread_new("rtpS", _UDPthread, NULL);
	}

	return ret;
}
/* --- */

static enum drtp_status _stop_listener_tcp(void) {
	enum drtp_status ret = DRTP_SUCCESS, ret2;
	int cam, connection;
	listener_active_tcp = FALSE;
	GThread *thread = listener_thread_tcp; listener_thread_tcp = NULL;
	ret2 = _close_listener_socket_tcp(); 
	g_thread_join(thread);

	for(cam = 0; cam < DRTP_MAX_CAMS; cam++)
	{
		for(connection = 0; connection < DRTP_MAX_CONN_PER_CAM; connection++)
		{
			if ((streams[cam].receivers[RECV_TCP][connection].socket >= 0))
			{
				char ip_addr[64];
				_call_app(RECV_TCP, cam, drtp_req_stop, 0, 0,  inet_ntop(AF_INET, &streams[cam].receivers[RECV_TCP][connection].dest.sin_addr, ip_addr, sizeof(ip_addr)));
				_teardown_socket(streams[cam].receivers[RECV_TCP][connection].socket);
			}
		}
	}
	return ret != DRTP_SUCCESS ? ret : ret2 ;
}
/* --- */

static enum drtp_status _stop_listener_udp(void) {
	enum drtp_status ret;
	listener_active_udp = FALSE;
	GThread *thread = listener_thread_udp; listener_thread_udp = NULL;
	ret = _close_listener_socket_udp(); 
	g_thread_join(thread);
	return ret;
}
/* --- */


/*
 * "PROTECTED" FUNCTIONS
 */

// TFM FIXME HACK ALERT for demo mode
#define RTP_DEMO_HACK
#ifdef RTP_DEMO_HACK
static unsigned short demo_port = 0;
static void _send_to_host_hack( void ) {

	uint32_t ssrc = _encode_ssrc(1, 720, 288);
	struct sockaddr_in incoming;
	char * ip = getenv("RTP_DEMO_RECEIVER_IP");
	char * port = getenv("RTP_DEMO_RECEIVER_PORT");
	enum drtp_status status;

	if ( NULL == ip )
		return;

	if (port) {
		demo_port = atoi ( port );
	} else {
		demo_port = 1338;
	}

	memset (&incoming, 0x0, sizeof(incoming));
	incoming.sin_family = AF_INET;
	incoming.sin_port = htons(demo_port);
	inet_aton(ip, &incoming.sin_addr);

	status =  _process_req(incoming, drtp_req_start, ssrc);
	if ( status == DRTP_SUCCESS ) {
		LOG_ERROR("DEMO MODE: Sending to address %s:%hd", ip, demo_port);
	} else {
		LOG_ERROR("DEMO MODE: Init failed with %s.", drtp_strerror(status));
	}
}
/* -- */
#else
#define _send_to_host_hack( )
#endif

enum drtp_status _drtps_init(void) {
	uint32_t i;

	if (initialised)
		return DRTP_SUCCESS;

	srandom(time(NULL));

	if( 0 != sem_init(&lock, 0, 1) ) {
		LOG_PERROR("sem_init");
		return DRTP_ERR_SYS;
	}

	memset (streams, 0x00, sizeof(streams));
	for (i=0; i<DRTP_MAX_CAMS; i++) {
		uint32_t j;
		for (j=0; j<DRTP_MAX_CONN_PER_CAM; j++) {
			streams[i].receivers[RECV_UDP][j].socket = -1;
			streams[i].receivers[RECV_TCP][j].socket = -1;
		}
	}

	initialised = 1;
	return DRTP_SUCCESS;
}
/* --- */

/*
 * PUBLIC FUNCTIONS
 */

enum drtp_status drtp_set_req_cb(drtp_req_cb new_callback, drtp_req_cb * old_callback) {
	drtp_req_cb _tmp_callback;
	enum drtp_status ret = DRTP_SUCCESS;

	enum drtp_status (*func)(void)= NULL;

	if (! initialised)
		return DRTP_ERR_NOT_INITIALIZED;

	/* ------------ thread starter logic ----*/
	if ( sem_wait( &lock ) ) {
		LOG_PERROR("sem_wait");
		return DRTP_ERR_SYS;
	}

	_tmp_callback = request_callback_udp;
	request_callback_udp = new_callback;

	if (request_callback_udp && ! _tmp_callback)
		func = _start_rtcp_listener;
	else if (! request_callback_udp && _tmp_callback)
		func = _stop_listener_udp;

	if (func)
	{
		if (DRTP_SUCCESS != (ret = func()))
			request_callback_udp = _tmp_callback;
	} else { /* RSR 20100628 added to avoid 0 calling */
		ret = DRTP_ERR_INVALID_ARG;
	}

	if ( sem_post( &lock ) ) {
		LOG_PERROR("sem_post");
		return DRTP_ERR_SYS;
	}
	/* ------------ thread starter logic ends */

	if (old_callback)
		*old_callback = _tmp_callback;

	_send_to_host_hack();

	return ret;
}
/* --- */


static enum drtp_status drtp_start_rtp_tcp_server_port(drtp_req_cb notify_cb, unsigned port)
{
	enum drtp_status ret = DRTP_SUCCESS;

	if (! initialised) { /* racy, but no problem */
		LOG_ERROR("DRTP_ERR_NOT_INITIALIZED\n");
		return DRTP_ERR_NOT_INITIALIZED;
	}
	if (listener_thread_tcp) { /* racy, but no problem */
		LOG_ERROR("DRTP_ERR_ALREADY_IN_PROG\n");
		return DRTP_ERR_ALREADY_IN_PROG;
	}

	/* ------------ thread starter logic ----*/
	if ( sem_wait( &lock ) ) {
		LOG_PERROR("sem_wait");
		return DRTP_ERR_SYS;
	}
	request_callback_tcp = notify_cb;
	ret = _open_listener_socket(NULL, port, SOCK_STREAM, &listener_socket_tcp);
	if (ret == DRTP_SUCCESS) {
		listener_active_tcp = TRUE;
		listener_thread_tcp = g_thread_new("rtpS", _TCPthread, GINT_TO_POINTER(port));
	}

	if ( sem_post( &lock ) ) {
		LOG_PERROR("sem_post");
		return DRTP_ERR_SYS;
	}
	/* ------------ thread starter logic ends */
	return ret;
}
/* --- */

enum drtp_status drtp_start_rtp_tcp_server(drtp_req_cb notify_cb)
{
	return drtp_start_rtp_tcp_server_port(notify_cb, DRTP_SERVER_PORT);
}

enum drtp_status drtp_start_rtp_tcp_local_server(drtp_req_cb notify_cb)
{
	return drtp_start_rtp_tcp_server_port(notify_cb, DRTP_SERVER_PORT+2);
}

enum drtp_status drtp_stop_rtp_tcp_server()
{
	enum drtp_status ret = DRTP_SUCCESS;

	if (! initialised || !listener_thread_tcp) {
		return DRTP_ERR_NOT_INITIALIZED;
	}

	/* ------------ thread stop logic ----*/
	if ( sem_wait( &lock ) ) {
		LOG_PERROR("sem_wait");
		return DRTP_ERR_SYS;
	}
	ret = _stop_listener_tcp();
	if ( sem_post( &lock ) ) {
		LOG_PERROR("sem_post");
		return DRTP_ERR_SYS;
	}
	/* ------------ thread stop logic ends */
	return ret;
}
/* --- */


static enum drtp_status _send_to_allUDP(uint32_t cam, struct rtp_header_t * hdr,
		uint32_t header_len, void * subheader, uint32_t subheader_len, void* data, uint32_t data_len) {
	unsigned int i;
	struct drtp_cam_info_t *stats;
	struct receivers_t * receivers;
	time_t now;

	stats     = _strm(cam)->stats[RECV_UDP];
	receivers = _strm(cam)->receivers[RECV_UDP];
	now       = time(NULL);

	for (i=0; i< DRTP_MAX_CONN_PER_CAM; i++) {
		int sock = receivers[i].socket;
		struct sockaddr_in * dest = &receivers[i].dest;
		int teardown = 0;
		enum drtp_status ret;

		if (0 > sock)
		{
			continue;
		}
		if (ntohl(hdr->ssrc) !=  receivers[i].ssrc) {
			hdr->ssrc = htonl(receivers[i].ssrc); /* in case a different format is sent */
		}

		if  (   ((int)header_len    != sendto(sock, hdr,  header_len, MSG_MORE,
						(struct sockaddr *) dest, sizeof(*dest)))
				||	((int)subheader_len != sendto(sock, subheader,  subheader_len, MSG_MORE,
						(struct sockaddr *) dest, sizeof(*dest)))
				||  ((int)data_len      != sendto(sock, data, data_len,   0,
						(struct sockaddr *) dest, sizeof(*dest)))) {
			LOG_PERROR("send");
			stats[i].tx_errors ++;
			if ( stats[i].tx_errors > DRTP_MAX_TX_ERRORS ) {
				LOG_ERROR( "Max number of TX errors reached (%u) for connection to %s:%u. Closing connection.\n",
						stats[i].tx_errors, inet_ntoa(dest->sin_addr),
						ntohs(dest->sin_port));
				teardown = 1;
			}
		} else {
			// TFM: FIXME HACK ALERT for demo mode
			if (
#ifdef RTP_DEMO_HACK
					( ntohs(receivers[i].dest.sin_port) != demo_port ) &&
#endif
					(receivers[i].last_seen + DRTP_KEEPALIVE_TIMEOUT) < now ) {
				LOG_ERROR( " No receiver report from %s:%u for %d seconds. Closing connection.\n",
						inet_ntoa(dest->sin_addr), ntohs(dest->sin_port),
						DRTP_KEEPALIVE_TIMEOUT);
				teardown = 1;
			} else {
				stats[i].bytes_sent += header_len + data_len;
				stats[i].frames_sent ++;
			}
		}
		if (teardown && DRTP_SUCCESS != (ret = _teardown_stream_udp(cam, ntohl(hdr->ssrc), dest)))
			LOG_ERROR( " ERROR while trying to stop stream of cam %d to %s:%u: #%d (%s).\n",
					cam, inet_ntoa(dest->sin_addr), ntohs(dest->sin_port),
					ret, drtp_strerror(ret));
	}

	return DRTP_SUCCESS;
}
/* --- */

/** Helper function to guarantee the len to write or an error */
static int _tcp_write(int sockFD, void *data, size_t len, const char* trace)
{
	size_t write_len;
	ssize_t res;
	for(write_len = 0; write_len < len; write_len += res) {
		res = write(sockFD, (uint8_t*)data + write_len, len - write_len);
		if (res <= 0)
		{
			LOG_ERROR( "Send error (%s)\n", trace);
			return res;
		}
	}
	return write_len;
}

static enum drtp_status _send_to_allTCP(uint32_t len, uint32_t cam,  uint32_t ssrc,
		uint64_t ts, void* data) {
	unsigned int i;
	struct drtp_cam_info_t* stats;
	struct receivers_t * receivers;

	stats     = _strm(cam)->stats[RECV_TCP];
	receivers = _strm(cam)->receivers[RECV_TCP];

	for (i=0; i< DRTP_MAX_CONN_PER_CAM; i++) {
		int sock = receivers[i].socket;
		struct sockaddr_in * dest = &receivers[i].dest;

		if (0 > sock)
			continue;

		if  (      ((int)sizeof(len)  != _tcp_write(sock, &len,  sizeof(len),  "Frame len"))
				|| ((int)sizeof(ssrc) != _tcp_write(sock, &ssrc, sizeof(ssrc), "Frame ssrc"))
				|| ((int)sizeof(ts)   != _tcp_write(sock, &ts, sizeof(ts),   "Frame timestamp"))
				|| ((int)len          != _tcp_write(sock, data,  len,          "Frame data"))
			)
		{
			char ip_addr[64];
			LOG_PERROR("send");
			if ( sem_wait( &lock ) ) {
				LOG_PERROR("sem_wait");
				return DRTP_ERR_SYS;
			}
			stats[i].tx_errors ++;
			_call_app(RECV_TCP, 0, drtp_req_stop, 0, 0, inet_ntop(AF_INET, &dest->sin_addr, ip_addr, sizeof(ip_addr)));
			LOG_ERROR( "TX errors for connection to %s:%u. Closing connection.\n",
					ip_addr, ntohs(dest->sin_port));
			_teardown_socket(sock);
			if ( sem_post( &lock ) ) {
				LOG_PERROR("sem_post");
				return DRTP_ERR_SYS;
			}
		} else {
			uint32_t header_len = sizeof( len ) + sizeof( ssrc) + sizeof( ts );
			stats[i].bytes_sent += header_len + len;
			stats[i].frames_sent ++;
		}

	}

	return DRTP_SUCCESS;
}
/* --- */


#if 0
static enum drtp_status _check_dump_frame(uint32_t cam, uint64_t timestamp,
		uint32_t w, uint32_t h, void * data, uint32_t len) {
	char filename[1024];
	unsigned short s = _strm(cam)->total_frames % MAX_FRAME_FILES;
	int fd;

	char * path = getenv(DRTP_FRAME_DROP_ENV_NAME);
	if (NULL == path)
		return DRTP_SUCCESS;

	sprintf(filename, "%s/frame_%i_%3.3d_%3.3d_%4.4X", path, cam, w, h, s);

#ifndef O_BINARY
#define O_BINARY 0
#endif
	fd = open(filename, O_RDWR | O_CREAT | O_TRUNC | O_BINARY, 0666 );
	if (fd >= 0) {
		if (    ( 8       != write(fd, &timestamp, 8))
				||  ((int)len != write(fd, data, len)) ) {
			LOG_ERROR("error writing frame data to file <%s>\n", filename);
		}
		close(fd);
	} else {
		LOG_ERROR("Could not open/create file <%s>", filename);
	}

	return DRTP_SUCCESS;
}
/* --- */
#endif

static enum drtp_status _check_before_send( uint32_t cam,
		uint32_t w, uint32_t h, uint32_t len, uint32_t num_receivers) {

	if (! initialised) {
		LOG_ERROR("DRTP_ERR_NOT_INITIALIZED\n");
		return DRTP_ERR_NOT_INITIALIZED;
	}
	if (!cam || (cam > DRTP_MAX_CAMS)) {
		LOG_ERROR("DRTP_ERR_INVALID_ARG (camera)\n");
		return DRTP_ERR_INVALID_ARG;
	}
	if ((w >= 0x7FF) || (h >= 0x3FF)) {
		LOG_ERROR("DRTP_ERR_INVALID_ARG (width/height)\n");
		return DRTP_ERR_INVALID_ARG;
	}
	if (len < 5) {
		LOG_ERROR("DRTP_ERR_INVALID_ARG (len)\n");
		return DRTP_ERR_INVALID_ARG;
	}

	if (1 > num_receivers )
		return DRTP_ERR_NO_CONN;

	return DRTP_SUCCESS;
}
/* --- */


enum drtp_status drtp_send_h264_tcp(uint32_t cam, uint32_t w, uint32_t h,
		uint64_t timestamp, void * data, uint32_t len) {

	enum drtp_status ret;
	uint32_t ssrc;

	ret = _check_before_send( cam, w, h, len, _strm(cam)->num_tcp_receivers );
	if ( DRTP_SUCCESS != ret )
		return ret;

	ssrc = _encode_ssrc(cam, w, h);

	/* len, ssrc, timestamp */
	ret = _send_to_allTCP( len, cam, ssrc, timestamp, data);

	_strm(cam)->total_frames ++;

	return ret;
}

enum drtp_status drtp_send_jpeg_rtp(uint32_t cam, uint32_t w, uint32_t h,
		uint64_t timestamp, void * data, uint32_t len, drtp_jpeg_specs_t *specs)
{
	enum drtp_status ret;

	uint8_t *buf;
	uint8_t *start;
	uint32_t ssrc;

	ret = _check_before_send( cam, w, h, len, _strm(cam)->num_udp_receivers );
	if ( DRTP_SUCCESS != ret )
		return ret;

	if (1 > _strm(cam)->num_udp_receivers)
		return DRTP_ERR_NO_CONN;

	buf = start = ( (uint8_t*)(data) );

	ssrc = _encode_ssrc(cam, w, h);

	if ( sem_wait( &lock ) ) {
		LOG_PERROR("sem_wait");
		return DRTP_ERR_SYS;
	}

	if (_strm(cam)->num_udp_receivers)
	{
		struct rtp_with_subheaders_t header;

#ifdef RTP_FRAGMENT
		while (buf < (start + len)) {
			uint32_t amount_left = (start + len) - buf;
			uint32_t chunk;
			int last;

			if (amount_left <= RTP_MAX_PAYLOAD_PER_PACKET) {
				chunk = amount_left;
				last = 1;
			} else {
				chunk = RTP_MAX_PAYLOAD_PER_PACKET;
				last = 0;
			}

			_gen_rtp_header(&header.rtp, ssrc, RTP_TYPE_JPEG, timestamp, _strm(cam)->sequence++, last);
			_gen_jpeg_header(&header.sub.jpeg, specs, buf - start);

			ret = _send_to_allUDP(cam, &header.rtp,
					sizeof(header.rtp), &header.sub.jpeg,
					sizeof(header.sub.jpeg), buf, chunk);

			buf += chunk;
		}
#else
		_gen_rtp_header(&header.rtp, ssrc, RTP_TYPE_JPEG, timestamp, _strm(cam)->sequence++, 1);
		_gen_jpeg_header(&header.sub.jpeg, specs, 0);
		ret = _send_to_allUDP(cam, &header, sizeof(header), buf, len);

		_strm(cam)->sequence ++;
#endif
	}

	_strm(cam)->total_frames ++;

	if ( sem_post( &lock ) ) {
		LOG_PERROR("sem_post");
		return DRTP_ERR_SYS;
	}

	return ret;
}

#if 0
enum drtp_status drtp_get_cam_info(uint32_t cam, struct drtp_cam_info_t info[DRTP_MAX_CAMS]) {

	if (cam > DRTP_MAX_CAMS)
		return DRTP_ERR_INVALID_ARG;

	memcpy(info, _strm(cam)->stats, sizeof(_strm(cam)->stats));

	return DRTP_SUCCESS;
}
#endif

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
