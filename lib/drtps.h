/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp sever interface
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

#ifndef _DRTPS_
#define _DRTPS_

#include <time.h>

/** Socket send buffer size of a streaming socket (UDP/TCP) */
#define DRTPS_SEND_BUFFER (512 * 1024)

/** Socket receive buffer size of the UDP RTCP socket */
#define DRTPS_RECEIVE_BUFFER (512 * 1024)

/** Maximum allowed client connection to the server */
#define DRTP_MAX_CONN_PER_CAM   9

/** RTCP/UDP port for RTP/UDP */
#define DRTP_SERVER_COMMAND_PORT 1337

/** RTP/TCP server port */
#define DRTP_SERVER_PORT 1336

/** Connections will be closed automatically by the library after
  * UDP send() failed this many times.
  */
#define DRTP_MAX_TX_ERRORS  (1000)

/** Connections will be closed automatically by the library if
  * the receiver has not sent a RTCP report for that many seconds.
  */
#define DRTP_KEEPALIVE_TIMEOUT   5

/** libdrtp request types */
enum drtp_request {
    drtp_req_start = 1,
    drtp_req_stop  = 2,
    drtp_req_info  = 3,
    drtp_req_alive = 4};

/** The status callback type definition; used by drtp_set_req_cb.
  * Callback will be run upon receiving drtp_req_start or drtp_req_stop requests.
  * \param cam camera "address" number range [1..DRTP_MAX_CAMS]
  * \param req request type; see enum drtp_request definition
  * \param width, \param height requested image resolution if req == drtp_req_start
  * \param client_address if not 0 (normally on connect notifications) it is a readable client address information
  * Only used for RTP/UDP for OSD handling.
  * This is a hack, should be replaced by RTSP or SDP.
  */
typedef enum drtp_status (*drtp_req_cb)(uint32_t cam, enum drtp_request req, uint32_t width, uint32_t height, const char* client_address);

/** Register a request processing callback with the library (UDP server with RTCP port DRTP_SERVER_COMMAND_PORT).
  * The library will spawn a receive thread and start receiving commands upon callback registration.
  * \param callback callback to be registered. May be NULL to unregister a
  *  callback, in which case the receive thread will terminate.
  * \param old_callback If non-NULL, then this is set to the previous callback pointer.
  * Don't use in combination with drtp_start_rtp_tcp_server.
  */
enum drtp_status drtp_set_req_cb(drtp_req_cb callback, drtp_req_cb * old_callback);

/** Start the RTP TCP server listening on port DRTP_SERVER_PORT.
  * The library will spawn a listen thread and send all frames to an open connection.
  * \param notify_cb notifications for client connection start and stop (mainly for tracing), can be 0.
  * Don't use in combination with drtp_set_req_cb.
  */
enum drtp_status drtp_start_rtp_tcp_server(drtp_req_cb notify_cb);

/** Start the RTP TCP server listening on port DRTP_SERVER_PORT+2 for local data transports.
  * The library will spawn a listen thread and send all frames to an open connection.
  * \param notify_cb notifications for client connection start and stop (mainly for tracing), can be 0.
  * Don't use in combination with drtp_set_req_cb.
  */
enum drtp_status drtp_start_rtp_tcp_local_server(drtp_req_cb notify_cb);

/** Stop the RTP TCP server listening on port DRTP_SERVER_PORT and close all open RTP connections.
 */
enum drtp_status drtp_stop_rtp_tcp_server();

/** Send an encoded video frame.
  * \param cam camera number the frame originated from range [1..DRTP_MAX_CAMS]
  * \param width, \param height resolution of the video frame
  * \param timestamp 64bit timestamp from dsaHeader
  * \param data video data to be sent
  * \param len amount of video data (in bytes) to be sent
  * Any frame is send to an open TCP connection,
  */
enum drtp_status drtp_send_h264_tcp(uint32_t cam, uint32_t width, uint32_t height,
                                    uint64_t timestamp, void * data, uint32_t len);

typedef struct drtp_jpeg_specs_s
{
	uint8_t type; 		/* JPEG over RTP type field */
	uint8_t q;		    /* JPEG over RTP quantization factor field */
	uint8_t width;		/* JPEG over RTP width field (8 pixel blocks) */
	uint8_t height;		/* JPEG over RTP heigth field (8 pixel blocks) */
} drtp_jpeg_specs_t;

/** Send an encoded video frame.
  * \param cam camera number the frame originated from range [1..DRTP_MAX_CAMS]
  * \param width, \param height resolution of the video frame
  * \param timestamp 64bit timestamp from dsaHeader
  * \param data video data to be sent
  * \param len amount of video data (in bytes) to be sent
  * \param specs JPEG over RTP header data
  * Any frame is send to an open RTP connection, whereas an RTP connection is open with a
  * open request (UDP) or a TCP connection. With TCP connections the call can block.
  * This function does not conform to RTP standard. The frame is embedded into a
  * H.264 package type SEI for transmittion with drtp_send_h264.
  */
enum drtp_status drtp_send_jpeg_rtp(uint32_t cam, uint32_t width, uint32_t height,
                                    uint64_t timestamp, void * data, uint32_t len, drtp_jpeg_specs_t *specs);

/** RTP server info query data structure
  */
enum drtp_active { inactive=0, active };

struct drtp_cam_info_t {
    uint32_t bytes_sent;
    uint32_t frames_sent;
    uint32_t tx_errors;
    enum drtp_active state;
    time_t last_started;
    time_t last_stopped;
};

/** Query rtp server information.
  * \param cam camera number from range [1..DRTP_MAX_CAMS]
  * \param info allocated RTP server info structure array
  * \depricated was never used
  */
// enum drtp_status drtp_get_cam_info(uint32_t cam, struct drtp_cam_info_t info[DRTP_MAX_CONN_PER_CAM]);

#endif /* _DRTPS_ */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
