#!/bin/sh
# SPDX-License-Identifier: LGPL-3.0-only
#
# Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
#
hexdump -ve '1/1 "%2.2x"' "$1" | sed -e 's/\(..\)/\\x\1/g'
