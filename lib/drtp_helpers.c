/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp internal helpers
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

/* $Id$ */

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/time.h>

#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include <errno.h>

#include <drtp.h>
#include "drtp_internal.h"



/*
 * "PROTECTED" FUNCTIONS
 */


enum drtp_status _str_to_sockaddr(const char * str, struct sockaddr_in * result, int default_port) {
    char * port;
    char addr[ strlen("255.255.255.255:65535") + 1 ];
    
    if (NULL == result)
        return DRTP_ERR_INVALID_ARG;

    strncpy(addr, str, sizeof(addr));
    addr[ sizeof(addr) - 1 ] = '\0';
	port = strchr(addr, ':');
	if (port) { *port++ = '\0'; }
    if (!port || (port[0] == 0)) {
		result->sin_port = htons(default_port);
	} else {
		result->sin_port = htons(atoi(port));
	}
    result->sin_family      = AF_INET;
    result->sin_addr.s_addr = inet_addr(addr);

    return DRTP_SUCCESS;
}
/* --- */

enum drtp_status _close_socket(int sock) {
    enum drtp_status ret = DRTP_SUCCESS;

    if (-1 < sock) {
        if (    (0 > shutdown(sock, SHUT_RDWR))
            &&  (errno != ENOTCONN) ) {
            LOG_PERROR("shutdown");
            ret = DRTP_ERR_SYS;
        }
        if (0 > close(sock)) {
            LOG_PERROR("close");
            ret = DRTP_ERR_SYS;
        }
    }

    return ret;
}
/* --- */

//do a nonblocking connect
//  return -1 on a system call error, 0 on success
//  sa - host to connect to, filled by caller
//  sock - the socket to connect
//  timeout - how long to wait to connect
static int conn_nonb(struct sockaddr_in* sa, int sock, int timeout)
{
	int flags = 0, error = 0, ret = 0;
	fd_set  rset, wset;
	socklen_t   len = sizeof(error);
	struct timeval  ts;

	ts.tv_sec = timeout;
	ts.tv_usec = 0;

	//clear out descriptor sets for select
	//add socket to the descriptor sets
	FD_ZERO(&rset);
	FD_SET(sock, &rset);
	wset = rset;    //structure assignment ok

	//set socket nonblocking flag
	if( (flags = fcntl(sock, F_GETFL, 0)) < 0)
		return -1;

	if(fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0)
		return -1;

	//initiate non-blocking connect
	if( (ret = connect(sock, (struct sockaddr *)sa, 16)) < 0 )
	{
		if (errno != EINPROGRESS)
			return -1;

		//we are waiting for connect to complete now
		if( (ret = select(sock + 1, &rset, &wset, NULL, (timeout) ? &ts : NULL)) < 0)
			return -1;
		if(ret == 0){   //we had a timeout
			errno = ETIMEDOUT;
			return -1;
		}

		//we had a positivite return so a descriptor is ready
		if (FD_ISSET(sock, &rset) || FD_ISSET(sock, &wset)){
			if(getsockopt(sock, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
				return -1;
		}else
			return -1;

		if(error){  //check if we had a socket error
			errno = error;
			return -1;
		}
	}

	//put socket back in blocking mode
	if(fcntl(sock, F_SETFL, flags) < 0)
		return -1;

	return 0;
}


enum drtp_status _open_sender_socket(int * sock, struct sockaddr_in * to, int tcp) {
    if (0 > (*sock = socket(AF_INET, (tcp ? SOCK_STREAM : SOCK_DGRAM), 0))) {
        LOG_PERROR("socket");
        return DRTP_ERR_SYS;
    }

	if (0 > conn_nonb(to, *sock, 5)) {
		_close_socket(*sock); *sock = -1;
        return DRTP_ERR_SYS;
    }

    int no_debug = 0;
    if (0 > setsockopt(*sock, SOL_SOCKET, SO_DEBUG, &no_debug, sizeof(no_debug))) {
        LOG_PERROR("setsockopt SO_DEBUG");
    }
    /* HYP-18744: enable 10s recv timeout in order to avoid forever blocking recv() socket calls
     * default value for SO_RCVTIMEO is 0, means it will never timeout. see https://linux.die.net/man/7/socket */
    struct timeval tv_recv = {.tv_sec = 10, .tv_usec = 0};
    if(0 > setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&tv_recv, sizeof(tv_recv))) {
        LOG_PERROR("setsockopt SO_RCVTIMEO");
        return DRTP_ERR_SYS;
    }

    return DRTP_SUCCESS;
}
/* --- */

enum drtp_status _open_listener_socket(char * ip_addr, unsigned short port, int type, int * sock) {
    struct sockaddr_in local_addr;
    enum drtp_status ret = DRTP_SUCCESS;
    int s;
	int reuse = 1;

    local_addr.sin_family      = AF_INET;
    local_addr.sin_port        = htons(port);
    local_addr.sin_addr.s_addr = ip_addr ? inet_addr(ip_addr) : INADDR_ANY;

    if (-1 < *sock)
        return DRTP_ERR_BUSY;

    if (0 > (s = socket( AF_INET, type, 0))) {
        LOG_PERROR("socket");
        return DRTP_ERR_SYS;
    }

    if (0 > setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse))) {
        LOG_PERROR("setsockopt SO_REUSEADDR");
        return DRTP_ERR_SYS;
    }

    if (0 > bind( s, (struct sockaddr*) &local_addr, sizeof(local_addr)) ) {
        LOG_PERROR("bind");
        _close_socket(s);
        return DRTP_ERR_SYS;
    }

	if (SOCK_STREAM == type) {
		if (listen(s, 8) < 0) {
			LOG_PERROR("listen");
        	_close_socket(s);
			return DRTP_ERR_SYS;
		}
	}

    *sock = s;

    return ret;
}
/* --- */

enum drtp_status _set_sock_bufs(int sock, uint32_t send_size, uint32_t recv_size) {
    enum drtp_status ret = DRTP_SUCCESS;

    if (    recv_size 
        &&  (0 > setsockopt(sock, SOL_SOCKET, SO_RCVBUFFORCE, &recv_size, sizeof(recv_size))) ) {
        LOG_PERROR("setsockopt(SO_RCVBUFFORCE)");
        ret = DRTP_ERR_SYS;
    }

    if (    send_size 
        &&  (0 > setsockopt(sock, SOL_SOCKET, SO_SNDBUFFORCE, &send_size, sizeof(send_size))) ) {
        LOG_PERROR("setsockopt(SO_SNDBUFFORCE)");
        ret = DRTP_ERR_SYS;
    }

    return ret;
}
/* --- */

enum drtp_status _get_sock_txlvl(int sock, uint32_t * tx_level) {
    enum drtp_status ret = DRTP_SUCCESS;
    uint32_t lvl = 0;

    if ( 0 > ioctl(sock, TIOCOUTQ, &lvl) ) {
        LOG_PERROR("ioctl(TIOCOUTQ)");
        ret = DRTP_ERR_SYS;
    }

    *tx_level = lvl;
    return ret;
}
/* --- */


int _fatal_receive_error(void) {
    switch (errno) {
        case EBADF:
        case EFAULT:
        case ENOTSOCK:
            return 1;
        default: break;
    }
    return 0;
}
/* --- */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 * 
 * NO CODE BELOW THIS! */
