/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  general rtp functions
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */


#include <drtp.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "drtp_internal.h"

static drtp_error_logger _error_logger = NULL;

void _drtp_log_error(char * str) {
    drtp_error_logger l = _error_logger;
    if (l) {
        l(str);
    } else {
        fwrite(str, strlen(str), 1, stderr);
        fflush(stderr);
    }
}
/* --- */

enum drtp_status drtp_init(void) {
	int res = 0;
    enum drtp_status ret;
	struct sigaction sa;

    if ( DRTP_SUCCESS != ( ret = _drtps_init() ))
        return ret;
    if ( DRTP_SUCCESS != ( ret = _drtpc_init() ))
        return ret;

	memset(&sa, 0, sizeof(sa));
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = SIG_IGN;
	res = sigaction(SIGPIPE, &sa, NULL);

	if (0 > res) {
		LOG_PERROR("sigaction SIGPIPE SIG_IGN");
		return DRTP_ERR_SYS;
	}
    return DRTP_SUCCESS;
}
/* --- */

const char * drtp_strerror(enum drtp_status s) {
    switch (s) {
        case DRTP_SUCCESS:              return "Success";
        case DRTP_ERR_NOT_INITIALIZED:  return "Library not initialized";
        case DRTP_ERR_BUSY:             return "Resource is busy";
        case DRTP_ERR_NO_MEM:           return "Memory exhausted";
        case DRTP_ERR_NO_CONN:          return "Not connected";
        case DRTP_ERR_INVALID_ARG:      return "Invalid argument in function call";
        case DRTP_ERR_SYS:              return "OS / System error";
        case DRTP_ERR_NOT_IMPLEMENTED:  return "Not implemented";
        case DRTP_ERR_ALREADY_IN_PROG:  return "Operation already in progress";
        case DRTP_ERR_UNKNOWN_CODEC:    return "The specified codec is unknown";
        case DRTP_ERR_UNKNOWN:          return "Unknown error";
        default:                        return "STRERROR ERROR: cannot resolve error number";
    }
}
/* --- */

void drtp_set_error_logger(drtp_error_logger logger, drtp_error_logger * old_logger) {
    if (old_logger)
        *old_logger = _error_logger;

    _error_logger = logger;
}

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
