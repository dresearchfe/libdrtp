/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  rtp internal helper
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

/* $Id$ */

#ifndef _DRTP_INTERNAL_
#define _DRTP_INTERNAL_

#include "drtp_rtp.h"
/*
 -------------------------------------------------  drtp.c
 */
/* Access to the custom application error logging callback */

void _drtp_log_error(char * str);

#define LOG_ERROR(...) {                 \
    char a[1024];                        \
    snprintf(a, sizeof(a), __VA_ARGS__); \
    _drtp_log_error(a);                  \
}

#define LOG_PERROR(str, ...) {                                          \
    char a[1024];                                                       \
    char b[1024];                                                       \
	if (strerror_r(errno,b, sizeof(b)) != 0) { strcpy(b, "error"); }     \
	snprintf(a, sizeof(a), "%s(%d): " str "\n", b, errno, ##__VA_ARGS__);    \
    _drtp_log_error(a);                                                 \
}

/*
 -------------------------------------------------  drtps.c
 */
/* Initialize RTP server part of the library */
enum drtp_status _drtps_init(void);

/*
 -------------------------------------------------  drtpc.c
 */
/** Initialize RTP client part of the library. This currently does nothing as there is nothing to initialize.
  */
enum drtp_status _drtpc_init(void);

/*
 -------------------------------------------------  drtp_parser.c
 */
/** Parse the RTCP header part of an incoming message.
  * \param data Message data.
  * \param len Message length.
  * \param req will be set to the drtp request type of this message.
  * \param ssrc will be set to the SSRC of this message.
  */
enum drtp_status _parse_rtcp(uint8_t * data, size_t len,
                        enum drtp_request * req, uint32_t * ssrc);

/** Generate RTCP header for requesting a stream start (H261 FIR header).
  * \param hdr pointer to an allocated struct rtcp_header_t to be filled by this call.
  * \param ssrc SSRC the request will refer to.
  */
void _gen_rtcp_stream_request( struct rtcp_header_t * hdr, uint32_t ssrc);

/** Generate RTCP header for requesting a stream stop (RTCP BYE header).
  * \param hdr pointer to an allocated struct rtcp_header_t to be filled by this call.
  * \param ssrc SSRC the request will refer to.
  */
void _gen_rtcp_bye_request( struct rtcp_header_t * hdr, uint32_t ssrc);

/** Generate RTCP receiver report header which is used by the library's server
  * for keepalive messages.
  * \param hdr pointer to an allocated struct rtcp_header_t to be filled by this call.
  * \param ssrc SSRC the request refers to.
  */
void _gen_rtcp_alive_request( struct rtcp_header_t * hdr, uint32_t ssrc);

/** Encode cam number, width, and height into SSRC.
  * \param cam camera number
  * \param width picture width
  * \param height picture height
  * \return SSRC
  * Implementation in drtp_parser.c
  */
uint32_t _encode_ssrc(uint32_t cam, uint32_t width, uint32_t height);

/** Decode cam number, width, and height from SSRC.
  * \param ssrc SSRC to decode
  * \param cam will be set to the camera number
  * \param width will be set to the picture width
  * \param height will be set to the picture height
  * Implementation in drtp_parser.c
  */
void _decode_ssrc(uint32_t ssrc, uint32_t *cam, uint32_t *width, uint32_t *height);

/** Parse the RTP header part of an incoming message.
  * \param data Message data.
  * \param len Message length.
  * \param rtp destination rtp header structure
  * \param ts will be set to the timestamp (in milliseconds).
  * \param flags will have set DRTP_FRAME_START and/or DRTP_FRAME_DONE if applicable
  *               and the codec type (see DRTP_CODEC_FROM_FLAGS).
  */
enum drtp_status _parse_rtp ( const void * data, size_t len, struct rtp_header_t * rtp,
                                    uint64_t *ts, uint32_t * flags);

/** Parse the JPEG header part of an incoming message. */
enum drtp_status _parse_jpeg( const void * data, size_t len,
                                uint32_t * flags, struct rtp_subheader_jpeg_t * jpeg);

/** Generate generic RTP header
  * \param result allocated struct rtp_header_t to store the data to.
  * \param ssrc SSRC this packet refers to.
  * \param cam camera number this packet refers to.
  * \param ts millisecond timestamp of this frame.
  * \param seq sequence number of this packet.
  * \param first != 0 if this is the first packet of this frame
  */
void _gen_rtp_header(struct rtp_header_t * result,
                        uint32_t ssrc, uint32_t cam, uint64_t ts, uint16_t seq,
                        uint32_t first);

/* generate RFC 2435 JPEG RTP header */
void _gen_jpeg_header(struct rtp_subheader_jpeg_t *result,
		drtp_jpeg_specs_t *specs, uint32_t fragm_offs);

/*
 -------------------------------------------------  drtp_helpers.c
 */
/** Shutdown, then close a socket.
  *  \param sock the socket handle
  */
enum drtp_status _close_socket(int sock);

#include <netinet/ip.h>

/** Convert a string IP / Port representation into a struct sockaddr_in.
  *  \param str ip in dotted decimal notation, colon, port. E.g.: "10.12.2.9:992"
  *  \param result allocated struct sockaddr_in to store the result to
  */
enum drtp_status _str_to_sockaddr(const char * str, struct sockaddr_in * result, int default_port);

/** Open a UDP socket and connect it to a remote host.
  *  \param sock resulting socket handle
  *  \param to remote host to connect to
  *  \param tcp true for TCP stream, false for UDP datagram
  */
enum drtp_status _open_sender_socket(int * sock, struct sockaddr_in * to, int tcp);

/** Open a UDP socket and bind it to a local port.
  *  \param ip_addr specific, local IP address to bind port to. May be NULL for IPADDR_ANY.
  *  \param port local port to be bound
  *  \param type the socket type (SOCK_STREAM, SOCK_DGRAM)
  *  \param sock resulting socket handle
  */
enum drtp_status _open_listener_socket(char * ip_addr, unsigned short port, int type, int * sock);

/** Set a socket's kernel receive and send buffer sizes.
  *  The setsockopt() call performed by this function requires CAP_NET_ADMIN privilege.
  *  \param sock the socket handle
  *  \param send_size send buffer size in bytes. May be 0 to not set the send buffer size.
  *  \param recv_size receive buffer size in bytes (actually this is _half_ of the receive buffer
  *                size in bytes as the kernel uses an internal flip buffer and will allocate
  *                (2 * recv_size) bytes of buffer memory).
  *                May be 0 to not set the receive buffer size.
  */
enum drtp_status _set_sock_bufs(int sock, uint32_t send_size, uint32_t recv_size);

/** Query a socket's transmit buffer level.
  *  \param sock the socket handle
  *  \param tx_level transmit buffer fill level (in bytes) will be written here.
  */
enum drtp_status _get_sock_txlvl(int sock, uint32_t * tx_level);

/** This function will consult ERRNO and return != 0 if a recv() error was fatal or not.
  */
int _fatal_receive_error(void);

#endif /* not defined_DRTP_INTERNAL_ */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
