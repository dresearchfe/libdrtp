/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  general rtp interface
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

#ifndef _DRTP_
#define _DRTP_

#include <stdint.h>

/** Cameras can be used with range [1..DRTP_MAX_CAMS] */
#define DRTP_MAX_CAMS   8

/** Define this to enable RTP MTU fragmentation. It should be enabled by default.  */
#define RTP_FRAGMENT

/** Version string of the drtp interface. */
const char* drtp_version();

/** libdrtp status codes */
enum drtp_status {    DRTP_SUCCESS             = 0,
                      DRTP_ERR_NOT_INITIALIZED = 1,
                      DRTP_ERR_BUSY            = 2,
                      DRTP_ERR_NO_MEM          = 3,
                      DRTP_ERR_NO_CONN         = 4,
                      DRTP_ERR_INVALID_ARG     = 5,
                      DRTP_ERR_SYS             = 6,
                      DRTP_ERR_NOT_IMPLEMENTED = 7,
                      DRTP_ERR_ALREADY_IN_PROG = 8,
                      DRTP_ERR_UNKNOWN_CODEC   = 9,
                      DRTP_ERR_UNKNOWN         = 99 };

/** Initialize the library - call this once at application startup.
  * This function will initialize drtp internal handles. It is required to be
  * called before any of the drtp_* functions can be called.
  * \return drtp_status error code.
  */
enum drtp_status drtp_init(void);

/** Resolve drtp_status code into human readable description.
  * \param status enum drtp_status code.
  * \return descriptive error string.
  */
const char * drtp_strerror(enum drtp_status status);

/** Library verbose error logging callback.
  * Install this callback to get called with verbose error reports from the library.
  */
typedef void (*drtp_error_logger)(const char *);
void drtp_set_error_logger(drtp_error_logger logger, drtp_error_logger * old_logger);

/** DEBUG: sender-side frame dump
 *   Set the environment variable to a directory. The library will write
 *   its frames to this directory. To e.g. use this with the test application, type:
$ DRTP_FRAME_DUMP_PATH="/tmp/frames" ./drtp_tst
 *
 * Frame names will be "frame_C_W_H_NNN", with
 *    C     the camera number
 *    W, H  the resolution
 *    NNN   the frame's number. Counting starts a t 0 and is done in hex.
 *
 * CAVEAT:
 *    THE FIRST 8 BYTES OF EACH FILE IS THE FRAME'S TIMESTAMP IN uint64_t
 *    LITTLE ENDIAN. STRIP THE FIRST 8 BYTES TO GET THE ORIGINAL FRAME.
 * CAVEAT ENDS.
 */
#define DRTP_FRAME_DROP_ENV_NAME "DRTP_FRAME_DUMP_PATH"

/* DEBUG: Maximum number of frame files to write. When MAX_FRAME_FILES is reached,
 * the oldest frames will be overwritten. Use the files' ctime or the embedded 8 byte
 * timestamps to determine the first and last frame written by the library.
 *
 * The maximum of the maximum is 65535 frames as the frame sequence counter is an unsigned short.
 */
#define MAX_FRAME_FILES 5000

#include <drtps.h>
#include <drtpc.h>

#endif /* _DRTP_ */

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 *
 * NO CODE BELOW THIS! */
