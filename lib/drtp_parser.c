/* Editor hints for vim
 * vim:set ts=4 sw=4 noexpandtab:  */
// SPDX-License-Identifier: LGPL-3.0-only
/**\file
 * \brief  internal rtp parser helper
 * \author Tilo Fromm
 * Copyright (C) 2011-2023 DResearch Fahrzeugelektronik GmbH
 *
 * $Id$
 */

#include <drtp.h>
#include "drtp_internal.h"
#include "drtp_rtp.h"

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

/*
 * PRIVATE FUNCTIONS
 */

static void _rtcp_to_host(struct rtcp_header_t * h) {
    h->bits.all = ntohs( h->bits.all );
    h->len      = ntohs( h->len );
    h->ssrc     = ntohl( h->ssrc );
}
/* --- */

static void _rtcp_to_net(struct rtcp_header_t * h) {
    h->bits.all = htons( h->bits.all );
    h->len      = htons( h->len );
    h->ssrc     = htonl( h->ssrc );
}
/* --- */

static void _rtp_to_host(struct rtp_header_t * h) {
    h->bits.all = ntohs( h->bits.all );
    h->sequence = ntohs( h->sequence );
    h->timestamp= ntohl( h->timestamp );
    h->ssrc     = ntohl( h->ssrc );
}
/* --- */

static void _rtp_to_net(struct rtp_header_t * h) {
    h->bits.all = htons( h->bits.all );
    h->sequence = htons( h->sequence );
    h->timestamp= htonl( h->timestamp );
    h->ssrc     = htonl( h->ssrc );
}
/* --- */

static uint32_t _ms_to_90kHz(uint64_t ms) {
    uint32_t _ms = ms;
    uint32_t res = _ms * 90;
    return res;
}
/* --- */

static uint64_t _90kHz_to_ms(uint32_t khz) {
    uint32_t res32 = khz / 90;
    uint64_t res = res32;
    return res;
}
/* --- */

/*
 * "PROTECTED" FUNCTIONS
 */

/* ------ R T C P ------ */

enum drtp_status _parse_rtcp(uint8_t * data, size_t len, enum drtp_request * req, uint32_t * ssrc) {
    struct rtcp_header_t rtcp;

    if ( len < sizeof(struct rtcp_header_t) )
        return DRTP_ERR_INVALID_ARG;

    memcpy(&rtcp, data, sizeof(rtcp));
    _rtcp_to_host(&rtcp);

    *ssrc = rtcp.ssrc;

    switch (rtcp.bits.bit.type) {
        case RTCP_TYPE_FIR: *req = drtp_req_start; break;
        case RTCP_TYPE_BYE: *req = drtp_req_stop;  break;
        case RTCP_TYPE_RR:  *req = drtp_req_alive; break;
        case RTCP_TYPE_APP: 
                            if (rtcp.bits.bit.mbz == RTCP_APPDEF_SUBTYPE_INFO) {
                                *req = drtp_req_info; 
                                break; }
                            /* fall through */
        default:            return DRTP_ERR_INVALID_ARG;
    }

    return DRTP_SUCCESS;
}
/* --- */

static void _gen_rtcp_request( struct rtcp_header_t * hdr, uint32_t ssrc, unsigned type) {
    memset(hdr, 0x00, sizeof(*hdr));
    hdr->bits.bit.version = 2;
    hdr->ssrc = ssrc;
    hdr->bits.bit.type = type;
    hdr->len  = 1; /* 32bit word count minus one; as specified by RFC 3550 */
    if (type == RTCP_TYPE_RR)
        hdr->bits.bit.mbz = 0;
    _rtcp_to_net(hdr);
}
/* --- */

void _gen_rtcp_stream_request( struct rtcp_header_t * hdr, uint32_t ssrc) {
    _gen_rtcp_request(hdr, ssrc, RTCP_TYPE_FIR);
}
/* --- */

void _gen_rtcp_bye_request( struct rtcp_header_t * hdr, uint32_t ssrc) {
    _gen_rtcp_request(hdr, ssrc, RTCP_TYPE_BYE);
}
/* --- */

void _gen_rtcp_alive_request( struct rtcp_header_t * hdr, uint32_t ssrc) {
    _gen_rtcp_request(hdr, ssrc, RTCP_TYPE_RR);
}
/* --- */


/* ------ S S R C ------ */

uint32_t _encode_ssrc(uint32_t cam, uint32_t width, uint32_t height) {
    return ((width & 0x7FF) << 13 | (height & 0x3FF) << 3 | ((cam-1) & 7) );
}
/* --- */

void _decode_ssrc(uint32_t ssrc, uint32_t *cam, uint32_t *width, uint32_t *height) {
    *width = (ssrc >> 13) & 0x7FF;
    *height= (ssrc >>  3) & 0x3FF;
    *cam   = (ssrc & 0x7)+1;
}
/* --- */

/* ------ R T P   P A R S I N G ------ */

/* parse RFC 3550 RTP header */
enum drtp_status _parse_rtp( const void * data, size_t len, struct rtp_header_t * rtp,
                            uint64_t *ts, uint32_t * flags) {
    *flags=0;

    if (sizeof (struct rtp_header_t) > len)
        return DRTP_ERR_INVALID_ARG;

	memcpy(rtp, data, sizeof(*rtp));
    _rtp_to_host(rtp);

    *ts   = _90kHz_to_ms(rtp->timestamp);

    if (rtp->bits.bit.marker)
        *flags |= DRTP_FRAME_DONE;

    if        (rtp->bits.bit.type == RTP_TYPE_JPEG)         {
        DRTP_SET_CODEC_FLAG( codec_jpeg, *flags );
    } else {
        return DRTP_ERR_UNKNOWN_CODEC;
    }

    return DRTP_SUCCESS;
}
/* --- */

/* parse JPEG subheader */
enum drtp_status _parse_jpeg( const void * data, size_t len, uint32_t * flags, 
        struct rtp_subheader_jpeg_t * jpeg) {

    if (sizeof( struct rtp_subheader_jpeg_t) > len)
        return DRTP_ERR_INVALID_ARG;

	memcpy(jpeg, data, sizeof(*jpeg));
    jpeg->bit.fragm_offs = ntohl(jpeg->bit.fragm_offs << 8);

    if( 0 == jpeg->bit.fragm_offs )
        *flags |= DRTP_FRAME_START;

    return DRTP_SUCCESS;
}
/* --- */

/* ------ R T P   G E N E R A T I O N ------ */

/* generate RFC 3550 RTP header */
void _gen_rtp_header(struct rtp_header_t * result,
                        uint32_t ssrc, uint32_t type, 
                        uint64_t ts, uint16_t seq,
                        uint32_t last) {

    memset(result, 0x00, sizeof(*result));
    result->bits.bit.version   = 2;
    result->bits.bit.marker    = !!last;
    result->bits.bit.type      = type;
    result->sequence  = seq;
    result->timestamp = _ms_to_90kHz(ts);
    result->ssrc      = ssrc;

    _rtp_to_net(result);
}
/* --- */

/* generate RFC 2435 JPEG RTP header */
void _gen_jpeg_header(struct rtp_subheader_jpeg_t *result,
		drtp_jpeg_specs_t *specs, uint32_t fragm_offs) {

    memset(result, 0x00, sizeof(*result));
    result->bit.type_spec = 0;
    result->bit.fragm_offs = htonl(fragm_offs) >> 8;
    result->bit.type = specs->type;
    result->bit.q = specs->q;
    result->bit.width = specs->width;
    result->bit.height = specs->height;
}

/* Editor hints for emacs
 *
 * Local Variables:
 * mode:c
 * c-basic-offset:4
 * indent-tabs-mode:t
 * tab-width:4
 * End:
 * 
 * NO CODE BELOW THIS! */
